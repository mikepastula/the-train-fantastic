package com.train.fantastic.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.util.List;

@Entity
@Table(name = "station")
public class Station {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long stationId;
    @Column(name = "stationName")
    @NotNull
    private String stationName;
    @Column(name = "timeGo")
    @NotNull
    private Time timeGo;
    @Column(name = "timeCome")
    @NotNull
    private Time timeCome;

    @ManyToMany(mappedBy = "stations")
    private List<Train> train;

    public Station(){

    }

    public Station(@NotNull String stationName, @NotNull Time timeGo, @NotNull Time timeCome, List<Train> train) {
        this.stationName = stationName;
        this.timeGo = timeGo;
        this.timeCome = timeCome;
        this.train = train;
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Time getTimeGo() {
        return timeGo;
    }

    public void setTimeGo(Time timeGo) {
        this.timeGo = timeGo;
    }

    public Time getTimeCome() {
        return timeCome;
    }

    public void setTimeCome(Time timeCome) {
        this.timeCome = timeCome;
    }

    public List<Train> getTrain() {
        return train;
    }

    public void setTrain(List<Train> train) {
        this.train = train;
    }
}
