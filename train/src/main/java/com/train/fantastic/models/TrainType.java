package com.train.fantastic.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "train_type", uniqueConstraints = { @UniqueConstraint(columnNames = "description") })
public class TrainType {
    @Id
    @Column(name = "train_type_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long trainTypeId;
    @Column(name = "speed")
    @NotNull
    private String speedType;
    @Column(name = "description")
    @NotNull
    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "trainType", cascade = CascadeType.REMOVE)
    private List<Train> train;

    public Long getTrainTypeId() {
        return trainTypeId;
    }

    public void setTrainTypeId(Long trainTypeId) {
        this.trainTypeId = trainTypeId;
    }

    public String getSpeedType() {
        return speedType;
    }

    public void setSpeedType(String speedType) {
        this.speedType = speedType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Train> getTrain() {
        return train;
    }

    public void setTrain(List<Train> train) {
        this.train = train;
    }

}
