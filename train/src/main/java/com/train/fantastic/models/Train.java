package com.train.fantastic.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "train", uniqueConstraints = { @UniqueConstraint(columnNames = "train_no") })
public class Train {
    @Id
    @Column(name = "train_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long trainId;
    @Column(name = "train_no")
    @NotNull
    private int trainNo;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "train_type_id", nullable = false)
    private TrainType trainType;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "train_station",
            joinColumns = { @JoinColumn(name = "train_id") },
            inverseJoinColumns = { @JoinColumn(name = "station_id") }
    )
    private List<Station> stations;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "train_day",
            joinColumns = { @JoinColumn(name = "train_id") },
            inverseJoinColumns = { @JoinColumn(name = "day_id") }
    )
    private List<Day> daysGo;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "train", cascade = CascadeType.REMOVE)
    private List<Carriage>  carriageList;

    public Train(){}

    public Train(long id){
        this.trainId=id;
    }

    public Train(@NotNull int trainNo,@NotNull TrainType trainType, List<Station> stations, List<Carriage> carriageList) {
        this.trainNo = trainNo;
        this.trainType = trainType;
        this.stations = stations;
        this.carriageList = carriageList;
    }

    public Long getTrainId() {
        return trainId;
    }

    public void setTrainId(Long trainId) {
        this.trainId = trainId;
    }

    public int getTrainNo() {
        return trainNo;
    }

    public void setTrainNo(int trainNo) {
        this.trainNo = trainNo;
    }

    public TrainType getTrainType() {
        return trainType;
    }

    public void setTrainType(TrainType trainType) {
        this.trainType = trainType;
    }

    public List<Station> getStationList() {
        return stations;
    }

    public void setStationList(List<Station> stations) {
        this.stations = stations;
    }

    public List<Carriage> getCarriageList() {
        return carriageList;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    public List<Day> getDaysGo() {
        return daysGo;
    }

    public void setDaysGo(List<Day> daysGo) {
        this.daysGo = daysGo;
    }

    public void setCarriageList(List<Carriage> carriageList) {
        this.carriageList = carriageList;
    }
}
