package com.train.fantastic.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "ticket")
public class Ticket {
    @Id
    @Column(name = "ticket_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ticketId;
    @Column(name = "sit_no")
    @NotNull
    private int sitNo;
    @Column(name = "day_go")
    @NotNull
    private Date dayGo;
    @Column(name = "day_come")
    @NotNull
    private Date dayCome;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "carriage_id", nullable = false)
    private Carriage carriage;

    public Ticket(){

    }

    public Ticket(@NotNull int sitNo, @NotNull Date dayGo, @NotNull Date dayCome, User user, Carriage carriage) {
        this.sitNo = sitNo;
        this.dayGo = dayGo;
        this.dayCome=dayCome;
        this.user = user;
        this.carriage = carriage;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public int getSitNo() {
        return sitNo;
    }

    public void setSitNo(int sitNo) {
        this.sitNo = sitNo;
    }

    public Date getDayCome() {
        return dayCome;
    }

    public void setDayCome(Date dayCome) {
        this.dayCome = dayCome;
    }

    public Date getDayGo() {
        return dayGo;
    }

    public void setDayGo(Date dayGo) {
        this.dayGo = dayGo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Carriage getCarriage() {
        return carriage;
    }

    public void setCarriage(Carriage carriage) {
        this.carriage = carriage;
    }
}
