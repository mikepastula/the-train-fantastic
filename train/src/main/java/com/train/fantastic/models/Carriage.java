package com.train.fantastic.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "carriage")
public class Carriage {
    @Id
    @Column(name = "carriage_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long carriageId;
    @Column(name = "carriage_no")
    @NotNull
    private int carriageNo;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "carriage_type_id", nullable = false)
    private CarriageType carriageType;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "carriage", cascade = CascadeType.REMOVE)
    private List<Ticket> ticket;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "train_id", nullable = false)
    private Train train;

    public Carriage(){

    }

    public Carriage(@NotNull int carriageNo, CarriageType carriageType, List<Ticket> ticket, Train train) {
        this.carriageNo = carriageNo;
        this.carriageType = carriageType;
        this.ticket = ticket;
        this.train = train;
    }

    public Long getCarriageId() {
        return carriageId;
    }

    public void setCarriageId(Long carriageId) {
        this.carriageId = carriageId;
    }

    public int getCarriageNo() {
        return carriageNo;
    }

    public void setCarriageNo(int carriageNo) {
        this.carriageNo = carriageNo;
    }

    public CarriageType getCarriageType() {
        return carriageType;
    }

    public void setCarriageType(CarriageType carriageType) {
        this.carriageType = carriageType;
    }

    public List<Ticket> getTicket() {
        return ticket;
    }

    public void setTicket(List<Ticket> ticket) {
        this.ticket = ticket;
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }
}
