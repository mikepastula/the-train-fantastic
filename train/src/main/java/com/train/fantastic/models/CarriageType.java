package com.train.fantastic.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "carriage_type")
public class CarriageType {
    @Id
    @Column(name = "carriage_type_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long carriageTypeId;
    @Column(name = "sits_count")
    @NotNull
    private int sitsCount;
    @Column(name = "price")
    @NotNull
    private float price;
    @Column(name = "image", columnDefinition="TEXT")
    private String image;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "carriageType", cascade = CascadeType.REMOVE)
    private List<Carriage> carriage;

    public CarriageType(){

    }

    public CarriageType(@NotNull int sitsCount, @NotNull float price, String image, List<Carriage> carriage) {
        this.sitsCount = sitsCount;
        this.price = price;
        this.image = image;
        this.carriage = carriage;
    }

    public Long getCarriageTypeId() {
        return carriageTypeId;
    }

    public void setCarriageTypeId(Long carriageTypeId) {
        this.carriageTypeId = carriageTypeId;
    }

    public int getSitsCount() {
        return sitsCount;
    }

    public void setSitsCount(int sitsCount) {
        this.sitsCount = sitsCount;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Carriage> getCarriage() {
        return carriage;
    }

    public void setCarriage(List<Carriage> carriage) {
        this.carriage = carriage;
    }
}
