package com.train.fantastic.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "discount", uniqueConstraints = { @UniqueConstraint(columnNames = "discountName") })
public class Discount {
    @Id
    @Column(name = "discount_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long discountId;
    @Column(name = "discountName")
    @NotNull
    private String discountName;
    @Column(name = "discountSum")
    @NotNull
    private float discountSum;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "discount", cascade = CascadeType.REMOVE)
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public float getDiscountSum() {
        return discountSum;
    }

    public void setDiscountSum(float discountSum) {
        this.discountSum = discountSum;
    }

}
