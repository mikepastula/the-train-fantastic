package com.train.fantastic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainfantasticApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrainfantasticApplication.class, args);
    }
}
