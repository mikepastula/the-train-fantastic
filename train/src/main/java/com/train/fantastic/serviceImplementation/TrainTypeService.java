package com.train.fantastic.serviceImplementation;

import com.train.fantastic.repository.ITrainTypeRepository;
import com.train.fantastic.request.TrainTypeRequest;
import com.train.fantastic.response.TrainTypeResponse;
import com.train.fantastic.service.ITrainTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TrainTypeService implements ITrainTypeService {
    @Autowired
    private ITrainTypeRepository trainTypeRepository;


    @Override
    public List<TrainTypeResponse> getAll() {
        return trainTypeRepository.findAll().stream().map(TrainTypeResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public TrainTypeResponse getById(long id) {
        return new TrainTypeResponse(trainTypeRepository.getOne(id));
    }

    @Override
    public TrainTypeResponse addTrainType(TrainTypeRequest trainTypeRequest) {
        return new TrainTypeResponse(trainTypeRepository.save(trainTypeRequest.initTrainType()));
    }

    @Override
    public TrainTypeResponse updateTrainType(TrainTypeRequest trainTypeRequest) {
        return new TrainTypeResponse(trainTypeRepository.saveAndFlush(trainTypeRequest.initTrainType()));
    }
}
