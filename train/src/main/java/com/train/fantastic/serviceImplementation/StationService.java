package com.train.fantastic.serviceImplementation;

import com.train.fantastic.repository.IStationRepository;
import com.train.fantastic.request.StationRequest;
import com.train.fantastic.response.StationResponse;
import com.train.fantastic.service.IStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StationService implements IStationService {
    @Autowired
    private IStationRepository stationRepository;

    @Override
    public List<StationResponse> getAll() {
        return stationRepository.findAll().stream().map(StationResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public StationResponse addStation(StationRequest stationRequest) {
        return new StationResponse(stationRepository.save(stationRequest.initStation()));
    }

    @Override
    public StationResponse updateStation(StationRequest stationRequest) {
        return null;
    }
}
