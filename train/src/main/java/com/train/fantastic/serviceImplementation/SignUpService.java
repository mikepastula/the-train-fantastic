package com.train.fantastic.serviceImplementation;

import com.train.fantastic.configAndUtils.ValidationUtils;
import com.train.fantastic.models.Discount;
import com.train.fantastic.models.Role;
import com.train.fantastic.models.User;
import com.train.fantastic.repository.IDiscountRepository;
import com.train.fantastic.repository.IRoleRepository;
import com.train.fantastic.repository.IUserRepository;
import com.train.fantastic.request.SignUpRequest;
import com.train.fantastic.response.UserResponse;
import com.train.fantastic.service.IEmailService;
import com.train.fantastic.service.ISignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class SignUpService implements ISignUpService {
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IRoleRepository roleRepository;
    @Autowired
    private IDiscountRepository discountRepository;
    @Autowired
    private IEmailService emailService;
    @Autowired
    public JavaMailSender emailSender;

    public String getRandomString() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    @Override
    public UserResponse signUp(SignUpRequest request) {
        if(ValidationUtils.isEmailAddressInvalid(request.getEmail())) {
            return null;
        }
        String password = getRandomString();

        Role r= roleRepository.getByRoleName("User");
        Discount d = discountRepository.getOne(5l);
        User savedUSer =userRepository.save(new User(request.getFirstname(), request.getLastname(),request.getSecondname(),
                request.getEmail(), password,request.getBirthday(), r, d,null));
        emailService.sendSimpleMessage(request.getEmail(),
                "Registraition on TrainFantastic ","Registration was successfull, your passowrd : " + password);
        return new UserResponse(savedUSer);
    }

}
