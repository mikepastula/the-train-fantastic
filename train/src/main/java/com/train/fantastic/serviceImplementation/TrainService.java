package com.train.fantastic.serviceImplementation;

import com.train.fantastic.models.Day;
import com.train.fantastic.models.Station;
import com.train.fantastic.models.Train;
import com.train.fantastic.models.TrainType;
import com.train.fantastic.repository.IDayRepository;
import com.train.fantastic.repository.IStationRepository;
import com.train.fantastic.repository.ITrainRepository;
import com.train.fantastic.repository.ITrainTypeRepository;
import com.train.fantastic.request.DayRequest;
import com.train.fantastic.request.TrainBuyRequest;
import com.train.fantastic.request.TrainRequest;
import com.train.fantastic.response.DayResponse;
import com.train.fantastic.response.TrainResponse;
import com.train.fantastic.service.ITrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TrainService implements ITrainService {
    @Autowired
    private ITrainRepository trainRepository;
    @Autowired
    private IStationRepository stationRepository;
    @Autowired
    private ITrainTypeRepository trainTypeRepository;
    @Autowired
    private IDayRepository dayRepository;

    @Override
    public List<TrainResponse> getAll() {
        List<Train> t=trainRepository.findAll();
        List<TrainResponse> t1=trainRepository.findAll().stream().map(TrainResponse::new)
                .collect(Collectors.toList());
        return t1;
    }

    @Override
    public TrainResponse getTrain(long id) {
        return new TrainResponse(trainRepository.getOne(id));
    }

    @Override
    public TrainResponse addTrain(TrainRequest trainRequest) {
        List<Day> days = new ArrayList<>();
        Train newTrain=trainRequest.initTrain();
        TrainType trainType = trainTypeRepository.getOne(trainRequest.getTrainType());
        Station stationFrom =stationRepository.getOne(trainRequest.getStationFrom());
        Station stationTo=stationRepository.getOne(trainRequest.getStationTo());
        newTrain.setTrainType(trainType);
        List<Station> stations = new ArrayList<>();
        stations.add(stationFrom);
        stations.add(stationTo);
        newTrain.setStationList(stations);
        if(trainRequest.getDays().startsWith("Понеділок")){

            days.add(dayRepository.getDayByDayName("Понеділок"));
            days.add(dayRepository.getDayByDayName("Середа"));
            days.add(dayRepository.getDayByDayName("Пятниця"));
            days.add(dayRepository.getDayByDayName("Неділя"));
        }else{
            days.add(dayRepository.getDayByDayName("Вівторок"));
            days.add(dayRepository.getDayByDayName("Четверг"));
            days.add(dayRepository.getDayByDayName("Субота"));
        }
        newTrain.setDaysGo(days);
        return new TrainResponse(trainRepository.save(newTrain));
    }

    @Override
    public TrainResponse updateTrain(TrainRequest trainRequest) {
        List<Day> days = new ArrayList<>();
        Train newTrain=trainRequest.initTrain();
        TrainType trainType = trainTypeRepository.getOne(trainRequest.getTrainType());
        Station stationFrom =stationRepository.getOne(trainRequest.getStationFrom());
        Station stationTo=stationRepository.getOne(trainRequest.getStationTo());
        newTrain.setTrainType(trainType);
        List<Station> stations = new ArrayList<>();
        stations.add(stationFrom);
        stations.add(stationTo);
        newTrain.setStationList(stations);
        if(trainRequest.getDays().startsWith("Понеділок")){
            days.add(dayRepository.getDayByDayName("Понеділок"));
            days.add(dayRepository.getDayByDayName("Середа"));
            days.add(dayRepository.getDayByDayName("Пятниця"));
            days.add(dayRepository.getDayByDayName("Неділя"));
        }else{
            days.add(dayRepository.getDayByDayName("Вівторок"));
            days.add(dayRepository.getDayByDayName("Четверг"));
            days.add(dayRepository.getDayByDayName("Субота"));
        }
        return new TrainResponse(trainRepository.saveAndFlush(newTrain));
    }

    @Override
    public void delete(Long id) {
        trainRepository.deleteById(id);
    }

    @Override
    public void canselTrain(DayRequest request) {
        Train tempTrain = trainRepository.getOne(request.getId());
    }

    @Override
    public List<DayResponse>  getDaysGo(Long trainId) {
        Train tempTrain = trainRepository.getOne(trainId);
        return tempTrain.getDaysGo().stream().map(DayResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<TrainResponse> getTrainsGo(DayRequest dayRequest) throws ParseException {
        Date tempDate = new SimpleDateFormat("dd/MM/yyyy").parse(dayRequest.getDate());
        Day tempDay = null;
        switch (tempDate.getDay()){
            case 1: tempDay=dayRepository.getDayByDayName("Понеділок");break;
            case 2: tempDay=dayRepository.getDayByDayName("Вівторок");break;
            case 3: tempDay=dayRepository.getDayByDayName("Середа");break;
            case 4: tempDay=dayRepository.getDayByDayName("Четверг");break;
            case 5: tempDay=dayRepository.getDayByDayName("Пятниця");break;
            case 6: tempDay=dayRepository.getDayByDayName("Субота");break;
            default: tempDay=dayRepository.getDayByDayName("Неділя");
        }
        return tempDay.getTrains().stream().map(TrainResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<TrainResponse> getTrainsByParams(TrainBuyRequest request) {
        Day tempDay = null;
        switch (request.getDate().getDay()){
            case 1: tempDay=dayRepository.getDayByDayName("Понеділок");break;
            case 2: tempDay=dayRepository.getDayByDayName("Вівторок");break;
            case 3: tempDay=dayRepository.getDayByDayName("Середа");break;
            case 4: tempDay=dayRepository.getDayByDayName("Четверг");break;
            case 5: tempDay=dayRepository.getDayByDayName("Пятниця");break;
            case 6: tempDay=dayRepository.getDayByDayName("Субота");break;
            default: tempDay=dayRepository.getDayByDayName("Неділя");
        }
        return tempDay.getTrains().stream().filter(train->request.getStationFrom()
                .equals(train.getStationList().get(0).getStationName())&&
                request.getStationTo().equals(train.getStationList().get(1).getStationName()))
                .map(TrainResponse::new).collect(Collectors.toList());
    }



}
