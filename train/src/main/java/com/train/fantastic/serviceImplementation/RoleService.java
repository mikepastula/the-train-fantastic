package com.train.fantastic.serviceImplementation;

import com.train.fantastic.models.Role;
import com.train.fantastic.repository.IRoleRepository;
import com.train.fantastic.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoleService implements IRoleService {
    @Autowired
    private IRoleRepository roleRepository;

    @Override
    public Role getRoleByName(String role) {
        return roleRepository.getByRoleName(role);
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.findAll();
    }
}
