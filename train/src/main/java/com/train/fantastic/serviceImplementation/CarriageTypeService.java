package com.train.fantastic.serviceImplementation;

import com.train.fantastic.models.CarriageType;
import com.train.fantastic.repository.ICarriageTypeRepository;
import com.train.fantastic.request.CarriageTypeRequest;
import com.train.fantastic.response.CarriageTypeResponse;
import com.train.fantastic.service.ICarriageTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarriageTypeService implements ICarriageTypeService {
    @Autowired
    private ICarriageTypeRepository carriageTypeRepository;


    @Override
    public List<CarriageTypeResponse> getAll() {
        return carriageTypeRepository.findAll().stream().map(CarriageTypeResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public CarriageType addCarriageType(CarriageTypeRequest carriageType) {
        return carriageTypeRepository.saveAndFlush(carriageType.initCarriageType());
    }

    @Override
    public CarriageType updateCarriageType(CarriageTypeRequest carriageType) {
        return carriageTypeRepository.saveAndFlush(carriageType.initCarriageType());
    }

    @Override
    public void delete(Long id) {
        carriageTypeRepository.deleteById(id);
    }
}
