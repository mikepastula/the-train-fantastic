package com.train.fantastic.serviceImplementation;

import com.train.fantastic.models.Ticket;
import com.train.fantastic.models.User;
import com.train.fantastic.repository.IDiscountRepository;
import com.train.fantastic.repository.IRoleRepository;
import com.train.fantastic.repository.IUserRepository;
import com.train.fantastic.request.UserRequest;
import com.train.fantastic.response.UserResponse;
import com.train.fantastic.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService implements IUserService {
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IRoleRepository roleRepository;
    @Autowired
    private IDiscountRepository discountRepository;

    @Override
    public UserResponse addUser(UserRequest user) {
        User userFromRequest=user.initUser();
        userFromRequest.setRole(roleRepository.getByRoleName(user.getRole()));
        userFromRequest.setDiscount(discountRepository.getOne(5L));
        User savedUser = userRepository.save(userFromRequest);
        return new UserResponse(savedUser);
    }

    @Override
    public void delete(long id) {
        userRepository.deleteById(id);
    }

    @Override
    public UserResponse getById(long id) {
        User user = userRepository.findById(id);
        return new UserResponse(user);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public UserResponse getUserByTicket(Ticket ticket) {
        return new UserResponse(userRepository.getOneByTicket(ticket));
    }

    @Override
    public UserResponse editUser(UserRequest user) {
        User oldUser =user.initUser();
        oldUser.setRole(roleRepository.getByRoleName(user.getRole()));
        oldUser.setDiscount(discountRepository.getByDiscountName(user.getDiscountName()));
        User savedUser = userRepository.saveAndFlush(oldUser);
        return new UserResponse(savedUser);
    }

    @Override
    public List<UserResponse> getAll() {
        return userRepository.findAll().stream().map(UserResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserResponse> getManagers() {
        return userRepository.findAll().stream().filter(user->"Manager".equals(user.getRole().getRoleName())).map(UserResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public UserResponse getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return new UserResponse(userRepository.findByEmail(authentication.getName()));
    }

}
