package com.train.fantastic.serviceImplementation;

import com.train.fantastic.models.*;
import com.train.fantastic.repository.*;
import com.train.fantastic.request.DayRequest;
import com.train.fantastic.request.TicketRequest;
import com.train.fantastic.request.TrainBuyRequest;
import com.train.fantastic.response.AvailableTicketsResponse;
import com.train.fantastic.response.TicketResponse;
import com.train.fantastic.service.ITicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TicketService implements ITicketService {
    @Autowired
    private ITicketRepository ticketRepository;
    @Autowired
    private ICarriageRepository carriageRepository;
    @Autowired
    private ITrainRepository trainRepository;
    @Autowired
    private IStationRepository stationRepository;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private EmailService emailService;

    @Override
    public List<TicketResponse> findByUserId(final Long id) {
        return ticketRepository.findByUser(new User(id)).stream().map(TicketResponse::new).collect(Collectors.toList());
    }

    @Override
    public Ticket getTicketById(long id) {
        return ticketRepository.getOne(id);
    }

    @Override
    public void deleteByTicketId(long id) {
        ticketRepository.deleteById(id);
    }

    @Override
    public List<AvailableTicketsResponse> getTicketsByParams(TrainBuyRequest request) {
        Station tempStation = stationRepository.findOneByStationName(request.getStationFrom());
        List<Train> tempTrains =tempStation.getTrain().stream()
                .filter(train->request.getStationTo().equals(train.getStationList().get(1).getStationName())).collect(Collectors.toList());
        ArrayList<AvailableTicketsResponse> available= new ArrayList<>();
        for(int i=0;i<tempTrains.size();i++){
            for(int j=0;j<tempTrains.get(i).getCarriageList().size();j++){
                List<Ticket> tempTickets=tempTrains.get(i).getCarriageList().get(j).getTicket().stream()
                        .filter(ticket -> request.getDate().equals(ticket.getDayGo())).collect(Collectors.toList());
                for(int k=0;k<tempTrains.get(i).getCarriageList().get(j).getCarriageType().getSitsCount();k++){
                    int finalI = k;
                    boolean contains = tempTickets.stream().anyMatch(ticket->ticket.getSitNo()== finalI);
                    if(!contains){
                        AvailableTicketsResponse tempTicket = new AvailableTicketsResponse();
                        tempTicket.setCarriageNo(tempTrains.get(i).getCarriageList().get(j).getCarriageNo());
                        tempTicket.setTrainNo(tempTrains.get(i).getTrainNo());
                        tempTicket.setTimeGo(tempStation.getTimeGo());
                        tempTicket.setPrice(tempTrains.get(i).getCarriageList().get(j).getCarriageType().getPrice());
                        tempTicket.setSitNo(k+1);
                        tempTicket.setTimeCome(tempTrains.get(i).getStations().get(1).getTimeCome());
                        available.add(tempTicket);
                    }
                }
            }
        }
        return available;
    }

    @Override
    public List<Integer> getAvailableTicketsByParams(DayRequest request) {
        Carriage tempCarriages=carriageRepository.getOne(request.getId());
        List<Ticket> tempTickets=tempCarriages.getTicket().stream()
                .filter(ticket -> request.getDate().equals(ticket.getDayGo())).collect(Collectors.toList());
        ArrayList<Integer> places = new ArrayList<>();
        for(int i=0;i<tempCarriages.getCarriageType().getSitsCount();i++){
            int finalI = i;
            boolean contains = tempTickets.stream().anyMatch(ticket->ticket.getSitNo()== finalI);
            if(!contains)
                places.add(i);
        }
        return places;
    }

    @Override
    public List<Integer> getBookedTicketsByParams(DayRequest request) {
        Carriage tempCarriages=carriageRepository.getOne(request.getId());
        List<Ticket> tempTickets=tempCarriages.getTicket().stream()
                .filter(ticket -> request.getDate().equals(ticket.getDayGo())).collect(Collectors.toList());
        ArrayList<Integer> places = new ArrayList<>();
        for(int i=0;i<tempCarriages.getCarriageType().getSitsCount();i++){
            int finalI = i;
            boolean contains = tempTickets.stream().anyMatch(ticket->ticket.getSitNo()== finalI);
            if(contains)
                places.add(i);
        }
        return places;
    }

    @Override
    public TicketResponse addTicket(TicketRequest ticketRequest,long userId) {
        Ticket ticket = new Ticket();
        ticket.setSitNo(ticket.getSitNo());
        User tmpUser=userRepository.getOne(userId);
        ticket.setUser(tmpUser);
        Train tempTrain = trainRepository.getOneByTrainNo(ticketRequest.getTrainNo());
        Carriage tempCarriage=carriageRepository.getOneByCarriageNoAndTrain(ticketRequest.getCarriageNo(),tempTrain);
        ticket.setCarriage(tempCarriage);
        Date dayGo = ticketRequest.getDayGo();
        dayGo.setHours(tempTrain.getStations().get(0).getTimeGo().getHours());
        dayGo.setMinutes(tempTrain.getStations().get(0).getTimeGo().getMinutes());
        ticket.setDayGo(dayGo);
        Calendar c = Calendar.getInstance();
        c.setTime(dayGo);
        c.add(Calendar.DATE, 1);
        Date dayCome = c.getTime();
        dayCome.setHours(tempTrain.getStations().get(1).getTimeGo().getHours());
        dayCome.setMinutes(tempTrain.getStations().get(1).getTimeGo().getMinutes());
        ticket.setDayCome(dayCome);
        Ticket savedTicket =ticketRepository.saveAndFlush(ticket);
        String message="Thank You for using our service for booking a ticket, you have booked tiket : place - "
                +savedTicket.getSitNo()+", carriage number - "+savedTicket.getCarriage().getCarriageNo()
                +", trainNumber - "+tempTrain.getTrainNo()+ ", date when train go - "+savedTicket.getDayGo()
                +", day when train come - "+savedTicket.getDayCome();
        emailService.sendSimpleMessage(tmpUser.getEmail(),"Ticket was booked",message);
        return new TicketResponse(savedTicket);
    }

}
