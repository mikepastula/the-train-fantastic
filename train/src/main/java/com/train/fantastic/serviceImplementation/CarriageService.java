package com.train.fantastic.serviceImplementation;

import com.train.fantastic.models.*;
import com.train.fantastic.repository.ICarriageRepository;
import com.train.fantastic.repository.ICarriageTypeRepository;
import com.train.fantastic.repository.ITicketRepository;
import com.train.fantastic.repository.ITrainRepository;
import com.train.fantastic.request.CarriageRequest;
import com.train.fantastic.request.DayRequest;
import com.train.fantastic.response.CarriageResponse;
import com.train.fantastic.service.ICarriageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarriageService implements ICarriageService {
    @Autowired
    private ICarriageRepository carriageRepository;
    @Autowired
    private ICarriageTypeRepository carriageTypeRepository;
    @Autowired
    private ITrainRepository trainRepository;
    @Autowired
    private ITicketRepository ticketRepository;
    @Autowired
    private EmailService emailService;


    @Override
    public List<CarriageResponse> getAll() {
        return carriageRepository.findAll().stream().map(CarriageResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public CarriageResponse getCarriage(long id) {
        return new CarriageResponse(carriageRepository.getOne(id));
    }

    @Override
    public List<CarriageResponse> getAllByTrain(long trainId) {
        Train t = trainRepository.getOne(trainId);
        return carriageRepository.getAllByTrainId(t).stream().map(CarriageResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public CarriageResponse addCarriage(CarriageRequest carriage) {
        Carriage c = carriage.initCarriage();
        CarriageType carriageType = carriageTypeRepository.getOne(carriage.getCarriageType());
        c.setTrain(trainRepository.getOne(carriage.getTrainId()));
        c.setCarriageType(carriageType);
        return new CarriageResponse(carriageRepository.saveAndFlush(c));
    }

    @Override
    public CarriageResponse updateCarriage(CarriageRequest carriage) {
        Carriage c = carriage.initCarriage();
        CarriageType carriageType = carriageTypeRepository.getOne(carriage.getCarriageType());
        c.setTrain(trainRepository.getOne(carriage.getTrainId()));
        c.setCarriageType(carriageType);
        return new CarriageResponse(carriageRepository.saveAndFlush(c));
    }

    @Override
    public void delete(Long id) {
        carriageRepository.deleteById(id);
    }

    @Override
    public void canselCarriage(DayRequest request) {
        Carriage c = carriageRepository.getOne(request.getId());
        List<Ticket> tempTickets = c.getTicket().stream()
                .filter(ticket -> request.getDate().equals(ticket.getDayGo())).collect(Collectors.toList());
        for(Ticket ticket:tempTickets){
            User tempUser = ticket.getUser();
            emailService.sendSimpleMessage(tempUser.getEmail(),
                    "Carriage canselation","Sorry to say that but your carriage was canseled, please book another ticket");
        }
        for (Ticket ticket:tempTickets) {
            ticketRepository.deleteById(ticket.getTicketId());
        }

    }


}
