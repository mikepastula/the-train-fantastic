package com.train.fantastic.controller;

import com.train.fantastic.request.DayRequest;
import com.train.fantastic.request.CarriageRequest;
import com.train.fantastic.request.StationRequest;
import com.train.fantastic.request.TrainRequest;
import com.train.fantastic.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("trainfantastic/manager")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
public class ManagerController {
    @Autowired
    private IUserService userService;
    @Autowired
    private ITrainService trainService;
    @Autowired
    private ICarriageService carriageService;
    @Autowired
    private ICarriageTypeService carriageTypeService;
    @Autowired
    private ITrainTypeService trainTypeService;
    @Autowired
    private IStationService stationService;
    @Autowired
    private IEmailService emailService;

    @GetMapping("/train/{id}")
    public ResponseEntity<?> getTrain(@PathVariable long id) {
        return ResponseEntity.ok(trainService.getTrain(id));
    }

    @GetMapping("/carraige/{id}")
    public ResponseEntity<?> getCarriage(@PathVariable long id) {
        return ResponseEntity.ok(carriageService.getCarriage(id));
    }

    @PostMapping("/create/train")
    public ResponseEntity<?> createTrain(@RequestBody TrainRequest request) {
        return ResponseEntity.ok(trainService.addTrain(request));
    }

    @PostMapping("/create/carriage")
    public ResponseEntity<?> createCarraige(@RequestBody CarriageRequest request) {
        return ResponseEntity.ok(carriageService.addCarriage(request));
    }

    @PutMapping("/update/train")
    public ResponseEntity<?> updateTrain(@RequestBody TrainRequest request) {
        return ResponseEntity.ok(trainService.updateTrain(request));
    }

    @PutMapping("update/carriage")
    public ResponseEntity<?> updateCarraige(@RequestBody CarriageRequest request) {
        return ResponseEntity.ok(carriageService.updateCarriage(request));
    }

    @PostMapping("create/station")
    public ResponseEntity<?> createStation(@RequestBody StationRequest request) {
        return ResponseEntity.ok(stationService.addStation(request));
    }

    @PostMapping("update/station")
    public ResponseEntity<?> updateStation(@RequestBody StationRequest request) {
        return ResponseEntity.ok(stationService.updateStation(request));
    }

    @DeleteMapping("cansel/train")
    public ResponseEntity<?> canselTrain(@RequestBody DayRequest request) {
        trainService.canselTrain(request);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("cansel/carriage")
    public ResponseEntity<?> canselCarriage(@RequestBody DayRequest request) {
        carriageService.canselCarriage(request);
        return ResponseEntity.ok(HttpStatus.OK);
    }



    @GetMapping("/cariagetypes")
    public ResponseEntity<?> getCarriageTypes() {
        return ResponseEntity.ok(carriageTypeService.getAll());
    }

    @GetMapping("/traintypes")
    public ResponseEntity<?> getTrainTypes() {
        return ResponseEntity.ok(trainTypeService.getAll());
    }

    @GetMapping("/daysByTrain/{id}")
    public ResponseEntity<?> getDaysByTrain(@PathVariable long id) {
        return ResponseEntity.ok(trainService.getDaysGo(id));
    }
}
