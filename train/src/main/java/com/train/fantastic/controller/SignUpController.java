package com.train.fantastic.controller;

import com.train.fantastic.request.SignUpRequest;
import com.train.fantastic.service.ISignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/signup")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
public class SignUpController {
    @Autowired
    public ISignUpService signUpService;

    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody SignUpRequest request) {
        return ResponseEntity.ok(signUpService.signUp(request));
    }

    public static HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true);
    }

}
