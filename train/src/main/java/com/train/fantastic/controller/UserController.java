package com.train.fantastic.controller;

import com.train.fantastic.request.TicketRequest;
import com.train.fantastic.request.UserRequest;
import com.train.fantastic.response.UserResponse;
import com.train.fantastic.service.ITicketService;
import com.train.fantastic.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("trainfantastic/my")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
public class UserController {
    @Autowired
    private IUserService userService;
    @Autowired
    private ITicketService ticketService;

    public static HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true);
    }

    @GetMapping()
    public ResponseEntity<?> getUser() {
        UserResponse user = userService.getCurrentUser();
        return ResponseEntity.ok(user);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable long id) {
        return ResponseEntity.ok(userService.getById(id));
    }

    @GetMapping("/logout")
    public ResponseEntity<?> logout() {
        session().invalidate();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody UserRequest userRequest) {
        return ResponseEntity.ok(userService.editUser(userRequest));
    }

    @PutMapping("/addTicket")
    public ResponseEntity<?> addTicket(@RequestBody TicketRequest ticketRequest) {
        return ResponseEntity.ok(ticketService.addTicket(ticketRequest,userService.getCurrentUser().getId()));
    }

    @GetMapping("/tickets")
    public ResponseEntity<?> getTicketsByUserId() {
        return ResponseEntity.ok(ticketService.findByUserId(userService.
                getCurrentUser().getId()));
    }

    @DeleteMapping("/tickets/{id}")
    public ResponseEntity<?> deleteTicketById(@PathVariable long id) {
        UserResponse user1 = userService.getCurrentUser();
        UserResponse user2 = userService.getUserByTicket(ticketService.getTicketById(id));
        if(user1.equals(user2)){
            ticketService.deleteByTicketId(id);
            return ResponseEntity.ok(HttpStatus.OK);
        }
        return ResponseEntity.ok("Ticket Not From Current User");
    }
}

