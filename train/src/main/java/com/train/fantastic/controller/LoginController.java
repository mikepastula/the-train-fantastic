package com.train.fantastic.controller;

import com.train.fantastic.models.User;
import com.train.fantastic.response.UserResponse;
import com.train.fantastic.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("trainfantastic/login")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
public class LoginController {
    @Autowired
    private IUserService userService;

    @GetMapping()
    public ResponseEntity<?> login() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof String) {
            String userName = (String) principal;
            User user = userService.getByEmail(userName);
            return ResponseEntity.ok(new UserResponse(user.getUserId(), user.getFirstname(),
                    user.getLastname(),user.getSecondname(),user.getEmail(),user.getBirthday(), user.getRole()));
        }
        return null;
    }

    @GetMapping("/error")
    public String errorHandle(Exception e) {
        return e.getMessage();
    }

}
