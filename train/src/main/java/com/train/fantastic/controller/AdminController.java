package com.train.fantastic.controller;

import com.train.fantastic.request.*;
import com.train.fantastic.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("trainfantastic/admin")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
public class AdminController {
    @Autowired
    private IUserService userService;
    @Autowired
    private ITrainService trainService;
    @Autowired
    private ICarriageService carriageService;
    @Autowired
    private ICarriageTypeService carriageTypeService;
    @Autowired
    private ITrainTypeService trainTypeService;

    @Autowired
    private IEmailService emailService;

    @GetMapping("/users")
    public ResponseEntity<?> getallUsers() {
        return ResponseEntity.ok(userService.getAll());
    }

    @PostMapping("create/user")
    public ResponseEntity<?> createUser(@RequestBody UserRequest request) {
        return ResponseEntity.ok(userService.addUser(request));
    }

    @PostMapping("create/traintype")
    public ResponseEntity<?> createTrainType(@RequestBody TrainTypeRequest request) {
        return ResponseEntity.ok(trainTypeService.addTrainType(request));
    }

    @PostMapping("update/traintype")
    public ResponseEntity<?> updateTrainType(@RequestBody TrainTypeRequest request) {
        return ResponseEntity.ok(trainTypeService.updateTrainType(request));
    }

    @PostMapping("create/carriagetype")
    public ResponseEntity<?> createCarriageType(@RequestBody CarriageTypeRequest request) {
        return ResponseEntity.ok(carriageTypeService.addCarriageType(request));
    }

    @PostMapping("update/carriagetype")
    public ResponseEntity<?> updateCarriageType(@RequestBody CarriageTypeRequest request) {
        return ResponseEntity.ok(carriageTypeService.updateCarriageType(request));
    }

    @PutMapping("update/user")
    public ResponseEntity<?> updateUser(@RequestBody UserRequest request) {
        return ResponseEntity.ok(userService.addUser(request));
    }

    @DeleteMapping("delete/user/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable long id) {
        userService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping("/managers")
    public ResponseEntity<?> getManagers() {
        return ResponseEntity.ok(userService.getManagers());
    }

    @DeleteMapping("/delete/train/{id}")
    public ResponseEntity<?> deleteTrain(@PathVariable long id) {
        trainService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("delete/carriage/{id}")
    public ResponseEntity<?> deleteCarraige(@PathVariable long id) {
        carriageService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("cansel")
    public ResponseEntity<?> deleteTrainType(@RequestBody DayRequest canselRequest) {
        return ResponseEntity.ok(HttpStatus.OK);
    }


}
