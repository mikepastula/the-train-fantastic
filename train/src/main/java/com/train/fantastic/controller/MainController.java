package com.train.fantastic.controller;

import com.train.fantastic.request.DayRequest;
import com.train.fantastic.request.TrainBuyRequest;
import com.train.fantastic.response.TrainResponse;
import com.train.fantastic.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("trainfantastic/")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
public class MainController {
    @Autowired
    private IUserService userService;
    @Autowired
    private ITrainService trainService;
    @Autowired
    private ICarriageService carriageService;
    @Autowired
    private ITicketService ticketService;
    @Autowired
    private IStationService stationService;

    @GetMapping("/trains")
    public ResponseEntity<?> getallTrains() {
        List<TrainResponse> t3=trainService.getAll();
        return ResponseEntity.ok(t3);
    }

    @GetMapping("/train/{id}/carriages")
    public ResponseEntity<?> getallCarriagesForTrain(@PathVariable long id) {
        return ResponseEntity.ok(carriageService.getAllByTrain(id));
    }

    @GetMapping("/stations")
    public ResponseEntity<?> getStations() {
        return ResponseEntity.ok(stationService.getAll());
    }

    @GetMapping("/carriages")
    public ResponseEntity<?> getallCarriages() {
        return ResponseEntity.ok(carriageService.getAll());
    }

    @PostMapping("/trainsByDate")
    public ResponseEntity<?> getTrainsByDate(@RequestBody DayRequest request) throws ParseException {
        return ResponseEntity.ok(trainService.getTrainsGo(request));
    }

    @PostMapping("/getTrainsByParams")
    public ResponseEntity<?> getTrainsByDate(@RequestBody TrainBuyRequest request) throws ParseException {
        return ResponseEntity.ok(trainService.getTrainsByParams(request));
    }

    @PostMapping("/getAllAvailableTickets")
    public ResponseEntity<?> getAvailableTickets(@RequestBody DayRequest request) throws ParseException {
        return ResponseEntity.ok(ticketService.getAvailableTicketsByParams(request));
    }

    @PostMapping("/getAllBookedTickets")
    public ResponseEntity<?> getbookedTickets(@RequestBody DayRequest request) throws ParseException {
        return ResponseEntity.ok(ticketService.getBookedTicketsByParams(request));
    }

    @PostMapping("/getAvailableTickets")
    public ResponseEntity<?> getTickets(@RequestBody TrainBuyRequest request) throws ParseException {
        return ResponseEntity.ok(ticketService.getTicketsByParams(request));
    }


}
