package com.train.fantastic.service;

import com.train.fantastic.models.Role;

import java.util.List;

public interface IRoleService {
    List<Role> getAll();
    Role getRoleByName(String role);
}
