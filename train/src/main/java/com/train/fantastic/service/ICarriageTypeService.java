package com.train.fantastic.service;

import com.train.fantastic.models.CarriageType;
import com.train.fantastic.request.CarriageTypeRequest;
import com.train.fantastic.response.CarriageTypeResponse;

import java.util.List;

public interface ICarriageTypeService {
    List<CarriageTypeResponse> getAll();
    CarriageType addCarriageType(CarriageTypeRequest carriageType);
    CarriageType updateCarriageType(CarriageTypeRequest carriageType);
    void delete(Long id);
}
