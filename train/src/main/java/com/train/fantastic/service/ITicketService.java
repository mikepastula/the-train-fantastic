package com.train.fantastic.service;

import com.train.fantastic.models.Ticket;
import com.train.fantastic.request.DayRequest;
import com.train.fantastic.request.TicketRequest;
import com.train.fantastic.request.TrainBuyRequest;
import com.train.fantastic.response.AvailableTicketsResponse;
import com.train.fantastic.response.TicketResponse;

import java.util.List;

public interface ITicketService {

    List<TicketResponse> findByUserId(Long id);

    Ticket getTicketById(long id);

    void deleteByTicketId(long id);

    List<AvailableTicketsResponse> getTicketsByParams(TrainBuyRequest request);

    List<Integer> getAvailableTicketsByParams(DayRequest request);

    List<Integer> getBookedTicketsByParams(DayRequest request);

    TicketResponse addTicket(TicketRequest ticketRequest,long userId);
}
