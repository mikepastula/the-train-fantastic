package com.train.fantastic.service;

import com.train.fantastic.request.DayRequest;
import com.train.fantastic.request.TrainBuyRequest;
import com.train.fantastic.request.TrainRequest;
import com.train.fantastic.response.DayResponse;
import com.train.fantastic.response.TrainResponse;

import java.text.ParseException;
import java.util.List;

public interface ITrainService {
    List<TrainResponse> getAll();
    TrainResponse getTrain(long id);
    TrainResponse addTrain(TrainRequest trainRequest);
    TrainResponse updateTrain(TrainRequest trainRequest);
    void delete(Long id);
    void canselTrain(DayRequest request);
    List<DayResponse> getDaysGo(Long id);
    List<TrainResponse> getTrainsGo(DayRequest date) throws ParseException;
    List<TrainResponse> getTrainsByParams(TrainBuyRequest request);
}
