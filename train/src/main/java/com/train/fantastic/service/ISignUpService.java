package com.train.fantastic.service;

import com.train.fantastic.request.SignUpRequest;
import com.train.fantastic.response.UserResponse;

public interface ISignUpService {

    public UserResponse signUp(SignUpRequest request);

}

