package com.train.fantastic.service;

import com.train.fantastic.models.Ticket;
import com.train.fantastic.models.User;
import com.train.fantastic.request.UserRequest;
import com.train.fantastic.response.UserResponse;

import java.util.List;

public interface IUserService {
    UserResponse addUser(UserRequest user);
    void delete(long id);
    UserResponse getById(long id) ;
    User getByEmail(String email) ;
    UserResponse getUserByTicket(Ticket ticket);
    UserResponse editUser(UserRequest user);
    List<UserResponse> getAll();
    List<UserResponse> getManagers();
    UserResponse getCurrentUser();
}
