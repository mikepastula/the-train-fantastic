package com.train.fantastic.service;

import com.train.fantastic.request.LoginRequest;

public interface ILoginService {
    String login(LoginRequest loginRequest);
}
