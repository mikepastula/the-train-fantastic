package com.train.fantastic.service;

import com.train.fantastic.request.StationRequest;
import com.train.fantastic.response.StationResponse;

import java.util.List;

public interface IStationService {
    List<StationResponse> getAll();
    StationResponse addStation(StationRequest stationRequest);
    StationResponse updateStation(StationRequest stationRequest);
}
