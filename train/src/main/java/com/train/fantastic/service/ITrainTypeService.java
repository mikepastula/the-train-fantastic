package com.train.fantastic.service;

import com.train.fantastic.request.TrainTypeRequest;
import com.train.fantastic.response.TrainTypeResponse;

import java.util.List;

public interface ITrainTypeService {
    List<TrainTypeResponse> getAll();
    TrainTypeResponse getById(long id);
    TrainTypeResponse addTrainType(TrainTypeRequest trainTypeRequest);
    TrainTypeResponse updateTrainType(TrainTypeRequest trainTypeRequest);
}
