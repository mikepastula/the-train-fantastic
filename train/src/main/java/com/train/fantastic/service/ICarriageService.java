package com.train.fantastic.service;

import com.train.fantastic.request.DayRequest;
import com.train.fantastic.request.CarriageRequest;
import com.train.fantastic.response.CarriageResponse;

import java.util.List;

public interface ICarriageService {
    List<CarriageResponse> getAll();
    CarriageResponse getCarriage(long id);
    List<CarriageResponse> getAllByTrain(long trainId);
    CarriageResponse addCarriage(CarriageRequest carriage);
    CarriageResponse updateCarriage(CarriageRequest carriage);
    void delete(Long id);
    void canselCarriage(DayRequest request);
}
