package com.train.fantastic.request;

import com.train.fantastic.models.Station;

import java.sql.Time;

public class StationRequest {
    private Long stationId;
    private String stationName;
    private Time timeGo;
    private Time timeCome;

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Time getTimeGo() {
        return timeGo;
    }

    public void setTimeGo(Time timeGo) {
        this.timeGo = timeGo;
    }

    public Time getTimeCome() {
        return timeCome;
    }

    public void setTimeCome(Time timeCome) {
        this.timeCome = timeCome;
    }

    public Station initStation(){
        Station s = new Station(getStationName(),getTimeGo(),getTimeCome(),null);
        if(getStationId()!=null)
            s.setStationId(getStationId());
        return s;
    }
}
