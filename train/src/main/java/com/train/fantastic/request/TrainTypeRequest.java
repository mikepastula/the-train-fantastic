package com.train.fantastic.request;

import com.train.fantastic.models.TrainType;

public class TrainTypeRequest {
    private Long trainTypeId;
    private String speedType;
    private String description;

    public Long getTrainTypeId() {
        return trainTypeId;
    }

    public void setTrainTypeId(Long trainTypeId) {
        this.trainTypeId = trainTypeId;
    }

    public String getSpeedType() {
        return speedType;
    }

    public void setSpeedType(String speedType) {
        this.speedType = speedType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TrainType initTrainType(){
        TrainType t =new TrainType();
        t.setDescription(getDescription());
        t.setSpeedType(getSpeedType());
        t.setTrainTypeId(getTrainTypeId());
        return t;
    }
}
