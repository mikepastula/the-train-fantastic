package com.train.fantastic.request;

import com.train.fantastic.models.Carriage;

public class CarriageRequest {
    private Long carriageId;
    private Long trainId;
    private Long carriageType;
    private int carriageNo;


    public Long getCarriageId() {
        return carriageId;
    }

    public void setCarriageId(Long carriageId) {
        this.carriageId = carriageId;
    }

    public Long getTrainId() {
        return trainId;
    }

    public void setTrainId(Long trainId) {
        this.trainId = trainId;
    }

    public int getCarriageNo() {
        return carriageNo;
    }

    public void setCarriageNo(int carriageNo) {
        this.carriageNo = carriageNo;
    }

    public Long getCarriageType() {
        return carriageType;
    }

    public void setCarriageType(Long carriageType) {
        this.carriageType = carriageType;
    }

    public Carriage initCarriage(){
        Carriage c =new Carriage(getCarriageNo(),null,null,null);
        if(getCarriageId()!=null)
            c.setCarriageId(getCarriageId());
        return c;
    }
}
