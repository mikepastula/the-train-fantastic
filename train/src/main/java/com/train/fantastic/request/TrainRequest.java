package com.train.fantastic.request;

import com.train.fantastic.models.Train;

public class TrainRequest {
    private Long trainId;
    private int trainNo;
    private long trainType;
    private long stationFrom;
    private long stationTo;
    private String days;

    public Long getTrainId() {
        return trainId;
    }

    public void setTrainId(Long trainId) {
        this.trainId = trainId;
    }

    public int getTrainNo() {
        return trainNo;
    }

    public void setTrainNo(int trainNo) {
        this.trainNo = trainNo;
    }

    public long getTrainType() {
        return trainType;
    }

    public void setTrainType(long trainTypeId) {
        this.trainType = trainTypeId;
    }

    public long getStationFrom() {
        return stationFrom;
    }

    public void setStationFrom(long stationFrom) {
        this.stationFrom = stationFrom;
    }

    public long getStationTo() {
        return stationTo;
    }

    public void setStationTo(long stationTo) {
        this.stationTo = stationTo;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public Train initTrain(){
        Train t = new Train(getTrainNo(),null,null,null);
        if(getTrainId()!=null)
            t.setTrainId(getTrainId());
        return t;
    }
}
