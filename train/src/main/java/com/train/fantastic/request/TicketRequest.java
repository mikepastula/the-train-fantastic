package com.train.fantastic.request;

import java.util.Date;

public class TicketRequest {
    private Long ticketId;
    private int sitNo;
    private Date dayGo;
    private int carriageNo;
    private int trainNo;
    private String from;
    private String to;

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public int getSitNo() {
        return sitNo;
    }

    public void setSitNo(int sitNo) {
        this.sitNo = sitNo;
    }

    public Date getDayGo() {
        return dayGo;
    }

    public void setDayGo(Date dayGo) {
        this.dayGo = dayGo;
    }

    public int getCarriageNo() {
        return carriageNo;
    }

    public void setCarriageNo(int carriageNo) {
        this.carriageNo = carriageNo;
    }

    public int getTrainNo() {
        return trainNo;
    }

    public void setTrainNo(int trainNo) {
        this.trainNo = trainNo;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
