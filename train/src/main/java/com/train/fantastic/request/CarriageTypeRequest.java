package com.train.fantastic.request;

import com.train.fantastic.models.CarriageType;

public class CarriageTypeRequest {

    private Long carriageTypeId;
    private int sitsCount;
    private float price;
    private String image;

    public Long getCarriageTypeId() {
        return carriageTypeId;
    }

    public void setCarriageTypeId(Long carriageTypeId) {
        this.carriageTypeId = carriageTypeId;
    }

    public int getSitsCount() {
        return sitsCount;
    }

    public void setSitsCount(int sitsCount) {
        this.sitsCount = sitsCount;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public CarriageType initCarriageType(){
        CarriageType c= new CarriageType(getSitsCount(),getPrice(),getImage(),null);
        c.setCarriageTypeId(getCarriageTypeId());
        return c;
    }
}
