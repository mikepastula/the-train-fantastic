package com.train.fantastic.configAndUtils;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class ValidationUtils {
    public static boolean validateNewPassword(String password, String old_password, String new_password, String confirm_new_password) {
        if(!password.equals(old_password))
            return false;
        if(!new_password.equals(confirm_new_password))
            return false;
        return true;
    }

    public static boolean isEmailAddressInvalid(String email) {
        boolean result = false;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = true;
        }
        return result;
    }

    public static boolean validateFirstName( String firstName ){
        return firstName.matches( "[A-Z][a-zA-Z]*" );
    }

    public static boolean validateLastName( String lastName ){
        return lastName.matches( "[a-zA-z]+([ '-][a-zA-Z]+)*" );
    }

}
