package com.train.fantastic.response;

import com.train.fantastic.models.Ticket;

import java.util.Date;

public class TicketResponse {
    private Long ticketId;
    private int sitNo;
    private Date dayGo;
    private Date dayCome;
    private long carriageNo;
    private long trainNo;
    private String where;
    private String from;
    private Float price;

    public TicketResponse(Long ticketId, int sitNo, Date dayGo, long carriageId, String where, String from) {
        this.ticketId = ticketId;
        this.sitNo = sitNo;
        this.dayGo = dayGo;
        this.where = where;
        this.from = from;
    }

    public TicketResponse(Ticket ticket) {
        this.ticketId = ticket.getTicketId();
        this.sitNo = ticket.getSitNo();
        this.dayGo = ticket.getDayGo();
        this.dayCome=ticket.getDayCome();
        this.carriageNo = ticket.getCarriage().getCarriageNo();
        this.trainNo=ticket.getCarriage().getTrain().getTrainNo();
        this.from=ticket.getCarriage().getTrain().getStationList().get(0).getStationName();
        this.where=ticket.getCarriage().getTrain().getStationList().get(1).getStationName();
        this.price=ticket.getCarriage().getCarriageType().getPrice();
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Date getDayCome() {
        return dayCome;
    }

    public void setDayCome(Date dayCome) {
        this.dayCome = dayCome;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public int getSitNo() {
        return sitNo;
    }

    public void setSitNo(int sitNo) {
        this.sitNo = sitNo;
    }

    public Date getDayGo() {
        return dayGo;
    }

    public void setDayGo(Date dayGo) {
        this.dayGo = dayGo;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public long getCarriageNo() {
        return carriageNo;
    }

    public void setCarriageNo(long carriageNo) {
        this.carriageNo = carriageNo;
    }

    public long getTrainNo() {
        return trainNo;
    }

    public void setTrainNo(long trainNo) {
        this.trainNo = trainNo;
    }
}
