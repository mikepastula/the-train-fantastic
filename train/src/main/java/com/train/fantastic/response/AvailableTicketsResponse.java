package com.train.fantastic.response;

import java.sql.Time;

public class AvailableTicketsResponse {
    private int sitNo;
    private int trainNo;
    private int carriageNo;
    private Time timeGo;
    private Time timeCome;
    private float price;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getSitNo() {
        return sitNo;
    }

    public void setSitNo(int sitNo) {
        this.sitNo = sitNo;
    }

    public int getTrainNo() {
        return trainNo;
    }

    public void setTrainNo(int trainNo) {
        this.trainNo = trainNo;
    }

    public int getCarriageNo() {
        return carriageNo;
    }

    public void setCarriageNo(int carriageNo) {
        this.carriageNo = carriageNo;
    }

    public Time getTimeGo() {
        return timeGo;
    }

    public void setTimeGo(Time timeGo) {
        this.timeGo = timeGo;
    }

    public Time getTimeCome() {
        return timeCome;
    }

    public void setTimeCome(Time timeCome) {
        this.timeCome = timeCome;
    }
}
