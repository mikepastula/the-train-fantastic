package com.train.fantastic.response;

import com.train.fantastic.models.TrainType;

public class TrainTypeResponse {
    private Long trainTypeId;
    private String speedType;
    private String description;

    public TrainTypeResponse(Long trainTypeId, String speedType, String description) {
        this.trainTypeId = trainTypeId;
        this.speedType = speedType;
        this.description = description;
    }

    public TrainTypeResponse(TrainType trainType) {
        this.trainTypeId = trainType.getTrainTypeId();
        this.speedType = trainType.getSpeedType();
        this.description = trainType.getDescription();
    }

    public Long getTrainTypeId() {
        return trainTypeId;
    }

    public void setTrainTypeId(Long trainTypeId) {
        this.trainTypeId = trainTypeId;
    }

    public String getSpeedType() {
        return speedType;
    }

    public void setSpeedType(String speedType) {
        this.speedType = speedType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
