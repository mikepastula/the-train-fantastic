package com.train.fantastic.response;

import com.train.fantastic.models.Train;
import com.train.fantastic.models.TrainType;

import java.sql.Time;

public class TrainResponse {
    private Long trainId;
    private int trainNo;
    private String speedType;
    private String description;
    private String stationFrom;
    private String stationTo;
    private Time timeCome;
    private Time timeGo;

    public TrainResponse(Long trainId, int trainNo, TrainType trainType, String stationFrom, String stationTo) {
        this.trainId = trainId;
        this.trainNo = trainNo;
        this.stationFrom=stationFrom;
        this.stationTo=stationTo;
        this.speedType = trainType.getSpeedType();
        this.description = trainType.getDescription();
    }

    public TrainResponse(Train train) {
        this.trainId = train.getTrainId();
        this.trainNo = train.getTrainNo();
        this.speedType = train.getTrainType().getSpeedType();
        this.description = train.getTrainType().getDescription();
        this.stationTo=train.getStationList().get(1).getStationName();
        this.stationFrom=train.getStationList().get(0).getStationName();
        this.timeCome=train.getStationList().get(1).getTimeCome();
        this.timeGo=train.getStationList().get(0).getTimeGo();
    }

    public Long getTrainId() {
        return trainId;
    }

    public void setTrainId(Long trainId) {
        this.trainId = trainId;
    }

    public int getTrainNo() {
        return trainNo;
    }

    public void setTrainNo(int trainNo) {
        this.trainNo = trainNo;
    }

    public String getSpeedType() {
        return speedType;
    }

    public void setSpeedType(String speedType) {
        this.speedType = speedType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStationFrom() {
        return stationFrom;
    }

    public void setStationFrom(String stationFrom) {
        this.stationFrom = stationFrom;
    }

    public String getStationTo() {
        return stationTo;
    }

    public void setStationTo(String stationTo) {
        this.stationTo = stationTo;
    }

    public Time getTimeCome() {
        return timeCome;
    }

    public void setTimeCome(Time timeCome) {
        this.timeCome = timeCome;
    }

    public Time getTimeGo() {
        return timeGo;
    }

    public void setTimeGo(Time timeGo) {
        this.timeGo = timeGo;
    }
}
