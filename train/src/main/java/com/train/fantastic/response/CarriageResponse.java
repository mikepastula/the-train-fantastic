package com.train.fantastic.response;

import com.train.fantastic.models.Carriage;

public class CarriageResponse {
    private Long carriageId;
    private int carriageNo;
    private int sitsCount;
    private float price;
    private String image;

    public CarriageResponse(Long carriageId, int carriageNo) {
        this.carriageId = carriageId;
        this.carriageNo = carriageNo;
    }

    public CarriageResponse(Carriage carriage) {
        this.carriageId = carriage.getCarriageId();
        this.carriageNo = carriage.getCarriageNo();
        this.sitsCount=carriage.getCarriageType().getSitsCount();
        this.price=carriage.getCarriageType().getPrice();
        this.image=carriage.getCarriageType().getImage();
    }

    public Long getCarriageId() {
        return carriageId;
    }

    public void setCarriageId(Long carriageId) {
        this.carriageId = carriageId;
    }

    public int getCarriageNo() {
        return carriageNo;
    }

    public void setCarriageNo(int carriageNo) {
        this.carriageNo = carriageNo;
    }

    public int getSitsCount() {
        return sitsCount;
    }

    public void setSitsCount(int sitsCount) {
        this.sitsCount = sitsCount;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
