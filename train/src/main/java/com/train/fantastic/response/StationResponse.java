package com.train.fantastic.response;

import com.train.fantastic.models.Station;

import java.sql.Time;

public class StationResponse {
    private Long stationId;
    private String stationName;
    private Time timeGo;
    private Time timeCome;

    public StationResponse(Station station) {
        this.stationId=station.getStationId();
        this.stationName=station.getStationName();
        this.timeCome=station.getTimeCome();
        this.timeGo=station.getTimeGo();
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Time getTimeGo() {
        return timeGo;
    }

    public void setTimeGo(Time timeGo) {
        this.timeGo = timeGo;
    }

    public Time getTimeCome() {
        return timeCome;
    }

    public void setTimeCome(Time timeCome) {
        this.timeCome = timeCome;
    }
}
