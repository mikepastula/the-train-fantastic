package com.train.fantastic.response;

import com.train.fantastic.models.Role;
import com.train.fantastic.models.User;

import java.util.Date;
import java.util.Objects;

public class UserResponse {

    private Long id;
    private String firstname;
    private String lastname;
    private String secondname;
    private String email;
    private Date birthday;
    private String role;
    private String discountName;
    private Float discountSum;

    public UserResponse(Long id, String firstname, String lastname,
                        String secondname, String email, Date birthday, Role role) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.secondname = secondname;
        this.email = email;
        this.birthday = birthday;
        this.role = role.getRoleName();
    }

    public UserResponse(User user){
        if(user !=null){
            this.id=user.getUserId();
            this.firstname=user.getFirstname();
            this.secondname=user.getSecondname();
            this.lastname=user.getLastname();
            this.email=user.getEmail();
            this.birthday=user.getBirthday();
            this.role=user.getRole().getRoleName();
            this.discountName=user.getDiscount().getDiscountName();
            this.discountSum=user.getDiscount().getDiscountSum();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role.getRoleName();
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public Float getDiscountSum() {
        return discountSum;
    }

    public void setDiscountSum(Float discountSum) {
        this.discountSum = discountSum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponse that = (UserResponse) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(firstname, that.firstname) &&
                Objects.equals(lastname, that.lastname) &&
                Objects.equals(secondname, that.secondname) &&
                Objects.equals(email, that.email) &&
                Objects.equals(birthday, that.birthday) &&
                Objects.equals(role, that.role) &&
                Objects.equals(discountName, that.discountName) &&
                Objects.equals(discountSum, that.discountSum);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstname, lastname, secondname, email, birthday, role, discountName, discountSum);
    }
}
