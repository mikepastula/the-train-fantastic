package com.train.fantastic.response;

import com.train.fantastic.models.CarriageType;

public class CarriageTypeResponse {
    private Long carriageTypeId;
    private int sitsCount;
    private float price;
    private String image;

    public CarriageTypeResponse(Long carriageTypeId, int sitsCount, float price, String image) {
        this.carriageTypeId = carriageTypeId;
        this.sitsCount = sitsCount;
        this.price = price;
        this.image = image;
    }

    public CarriageTypeResponse(CarriageType carriageType) {
        this.carriageTypeId = carriageType.getCarriageTypeId();
        this.sitsCount = carriageType.getSitsCount();
        this.price = carriageType.getPrice();
        this.image = carriageType.getImage();
    }

    public Long getCarriageTypeId() {
        return carriageTypeId;
    }

    public void setCarriageTypeId(Long carriageTypeId) {
        this.carriageTypeId = carriageTypeId;
    }

    public int getSitsCount() {
        return sitsCount;
    }

    public void setSitsCount(int sitsCount) {
        this.sitsCount = sitsCount;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}