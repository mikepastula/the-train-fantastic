package com.train.fantastic.response;

import com.train.fantastic.models.Day;

public class DayResponse {
    String dayName;

    public DayResponse(String dayName) {
        this.dayName = dayName;
    }

    public DayResponse(Day day) {
        this.dayName = day.getDayName();
    }

    public String getdayName() {
        return dayName;
    }

    public void setdayName(String dayName) {
        dayName = dayName;
    }
}
