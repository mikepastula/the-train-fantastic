package com.train.fantastic.repository;

import com.train.fantastic.models.Discount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDiscountRepository extends JpaRepository<Discount, Long> {
    Discount getByDiscountName(String discountName);
}
