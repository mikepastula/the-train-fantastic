package com.train.fantastic.repository;

import com.train.fantastic.models.CarriageType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICarriageTypeRepository  extends JpaRepository<CarriageType, Long> {
    CarriageType getOneBySitsCountAndPrice(int sitsCount,float price);
    CarriageType getOneBySitsCount(int sitsCount);
}
