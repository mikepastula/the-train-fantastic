package com.train.fantastic.repository;

import com.train.fantastic.models.Day;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDayRepository extends JpaRepository<Day, Long> {
    //List<Day> findAllByTrain(Train train);

    Day getDayByDayName(String dayName);
}
