package com.train.fantastic.repository;

import com.train.fantastic.models.Ticket;
import com.train.fantastic.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository  extends JpaRepository<User, Long> {
    User findByEmail(String email);
    User findById(long id);
    User getOneByTicket(Ticket ticket);

}
