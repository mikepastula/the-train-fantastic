package com.train.fantastic.repository;

import com.train.fantastic.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleRepository extends JpaRepository<Role, Long> {

    Role getByRoleName(String roleName);
}
