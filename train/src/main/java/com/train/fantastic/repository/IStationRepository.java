package com.train.fantastic.repository;

import com.train.fantastic.models.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Time;

@Repository
public interface IStationRepository  extends JpaRepository<Station, Long> {
    Station findOneByStationNameAndTimeCome(String stationName, Time timeCome);
    Station findOneByStationNameAndTimeGo(String stationName, Time timeGo);
    Station findOneByStationName(String stationName);
}
