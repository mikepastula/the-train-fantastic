package com.train.fantastic.repository;

import com.train.fantastic.models.TrainType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITrainTypeRepository extends JpaRepository<TrainType, Long> {
    TrainType findOneBySpeedTypeAndDescription(String speedType, String description);
}
