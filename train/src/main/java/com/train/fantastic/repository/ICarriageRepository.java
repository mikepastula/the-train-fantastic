package com.train.fantastic.repository;

import com.train.fantastic.models.Carriage;
import com.train.fantastic.models.Train;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ICarriageRepository  extends JpaRepository<Carriage, Long> {

    @Query("SELECT c FROM Carriage c WHERE c.train = :train")
    List<Carriage> getAllByTrainId(@Param("train")Train train);

    Carriage getOneByCarriageNoAndTrain(int carriageNo,Train train);
}
