package com.train.fantastic.repository;

import com.train.fantastic.models.Ticket;
import com.train.fantastic.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITicketRepository  extends JpaRepository<Ticket, Long> {
    List<Ticket> findByUser(User user);

}

