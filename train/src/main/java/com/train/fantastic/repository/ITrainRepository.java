package com.train.fantastic.repository;

import com.train.fantastic.models.Train;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ITrainRepository extends JpaRepository<Train, Long> {
    Train getOneByTrainNo(int trainNo);
}
