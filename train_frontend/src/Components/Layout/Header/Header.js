import React from 'react';
import Logo from './logo.png'
import { NavLink } from 'react-router-dom';

export default (props) => {
    return (
        <div className="container">
            <header className="blog-header py-3">
                <div className="row flex-nowrap justify-content-between align-items-center">
                    <div className="col-3 pt-1">
                        <NavLink className="text-muted" to="/">
                            <img height="75" src={Logo} alt="Logo"/>
                        </NavLink>
                    </div>
                    <div className="col-4 text-center">
                        <nav className="my-2 my-md-0 mr-md-3">

                            <NavLink to="/" > Головна сторінка </NavLink>
                            <NavLink to="/feedback" > Зворотній зв'язок </NavLink>
                            <NavLink to="/account" > Особистий кабінет </NavLink>
                            <NavLink to="/search" > Купити квиток </NavLink>
                        </nav>
                    </div>
                    <div className="col-5 d-flex justify-content-end align-items-center">
                        <form className="form-inline my-2 my-lg-0">
                            <input className="form-control mr-sm-3" type="text" placeholder="Введіть номер потяга" aria-label="Search"/>
                            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Пошук</button>
                        </form>
                    </div>
                </div>
                <hr />
            </header>
        </div>
    )
}