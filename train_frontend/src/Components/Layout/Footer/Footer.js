import React from 'react';

export default (props) => {
    return (
        <footer className="pt-4 my-md-5 pt-md-5 border-top">
            <div className="row">
                <div className="col-12 col-md">
                    <img className="mb-2" src="img/logo.jpeg" alt="" width="24" height="24"/>
                    <small className="d-block mb-3 text-muted">© 2018-2019</small>
                </div>
                <div className="col-6 col-md">
                    <h5>Навігація</h5>
                    <ul className="list-unstyled text-small">
                        <li><a className="text-muted" href="#">Потяги</a></li>
                        <li><a className="text-muted" href="#">Головна</a></li>
                        <li><a className="text-muted" href="#">Особистий кабінет</a></li>
                    </ul>
                </div>
                <div className="col-6 col-md">
                    <h5>Посилання</h5>
                    <ul className="list-unstyled text-small">
                        <li><a className="text-muted" href="#">Ми в фейсбуці</a></li>
                    </ul>
                </div>
                <div className="col-6 col-md">
                    <h5>Інформація</h5>
                    <ul className="list-unstyled text-small">
                        <li><a className="text-muted" href="#">Зворотній зв'язок</a></li>
                    </ul>
                </div>
            </div>
</footer>
    )
}