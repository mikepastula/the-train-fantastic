import React from 'react';
import { NavLink } from 'react-router-dom';
import Signin from '../../Authentication/Signin/Signin';

export default (props) => {
    return (
        <div className="col-md-3">
            <form className="form-signin">
                <Signin />
                <div className="form-group">
                    <br />
                    <p className="text">Do not have account ?</p>
                    <NavLink to='/signup' className="btn btn-outline-primary form-control">Sign up</NavLink>
                </div>
            </form>
        </div>
    )
};