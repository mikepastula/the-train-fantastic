import React from 'react';
import { NavLink } from 'react-router-dom';
import cardImg from './cardImg.jpg'

export default (props) => {
    const {train} = props;
    return (
        <div className="col-md-12 news-list">
            <div className="card flex-md-row mb-4 shadow-sm h-md-250">
                <div className="card-body d-flex flex-column align-items-start">
                    <strong className="d-inline-block mb-2 text-primary">From : {train.stationFrom}, Start at : {train.timeGo}</strong>
                    <strong className="d-inline-block mb-2 text-primary">To : {train.stationTo}, End at : {train.timeCome}</strong>
                    <h3 className="mb-0">
                        Train number : {train.trainNo}
                    </h3>
                    <p>Speed type : {train.speedType}</p>
                    <p className="card-text mb-auto">{train.description}</p>
                    <NavLink to={`/train/${train.trainId}`}>Continue reading</NavLink>
                </div>
                <img className="card-img-right flex-auto d-none d-lg-block"
                    alt="Thumbnail [200x250]"
                    src={cardImg}
                    data-holder-rendered="true" />
            </div>
        </div>
    )
}