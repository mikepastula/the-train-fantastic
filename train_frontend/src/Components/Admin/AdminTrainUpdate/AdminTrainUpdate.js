import React , { Component } from 'react';
import axios from '../../../utils/axios';
import {
    Row,
    Col,
    Grid,
    Button
} from 'react-bootstrap';

class AdminUpdateTrain extends Component {
    state = {
        "trainId":72,
        "trainNo": 226,
        "trainType":2,
        "stationFrom":1,
        "stationTo":2,
        days: []
        // trainNo: null,
        // trainType : 123,
            // stationFrom : '',
            // stationTo : '',
        // description: '',
        // timeToGo: '',
        // timeToCome: '',
        // trainId: '',
    }

    inputHandler(event) {
        const { target } = event;
        console.log('target', target);
        this.setState({
            [target.name]: target.value
        });
    }

    submitHandler(event) {
        event.preventDefault();
        const data = this.state;
        data.trainNo = parseInt(this.state.trainNo);
        console.log('submit handler',data);
        axios.put('trainfantastic/manager/update/train', data, {
            headers: {
                'Authorization': `Basic ${localStorage.getItem('credentials')}`
            }
        })
            .then(response => {
                console.log(response)
            })
            .catch(err => {
                console.log(err);
            })
    }

    componentDidMount() {
        axios.get(`trainfantastic/manager/train/${this.props.match.params.trainId}`,{
            headers: {
                'Authorization': `Basic ${localStorage.getItem('credentials')}`
            }
        })
            .then(response => {
                console.log(response);
                const { data } = response;
                // this.setState({
                //     stationTo: data.stationTo,
                //     stationFrom: data.stationFrom,
                //     trainNo: data.trainNo,
                //     timeToGo: data.timeGo,
                //     timeToCome: data.timeToCome,
                //     speedType: data.speedType,
                //     description: data.description
                // })
            })
            .catch(err => {
                console.log(err);
                this.props.history.push('/admin/')
            })
    }

    // componentWillMount() {
    //     this.setState({
    //         trainId: this.props.match.params.trainId
    //     })
    // }

    render() {
        return(
            <Grid>
                <Row>
                    <Col md={6} className="offset-md-3">
                        <div className="mb-3">
                            <label for="trainNo">train Number</label>
                            <input name="trainNo" id="trainNo" type="number" value={this.state.trainNo} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div>
                        <div className="mb-3">
                            <label for="stationTo">station To</label>
                            <input name="stationTo" id="stationTo" type="text" value={this.state.stationTo} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div>
                        <div className="mb-3">
                            <label for="stationFrom">Station from</label>
                            <input name="stationFrom" id="stationFrom" type="text" value={this.state.stationFrom} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div>
                        <select onChange={this.inputHandler.bind(this)} className="form-control" name="days" multiple>
                            <option value="Понеділок">Понеділок</option>
                            <option value="Середа">Сеереда</option>
                            <option value="Пятниця">пятниця</option>
                            <option value="Неділя">Неділя</option>
                        </select>
                        <select onChange={this.inputHandler.bind(this)} className="form-control" name="days" multiple>
                            <option value="Вівторок">Вівторок</option>
                            <option value="Четврег">Четврег</option>
                            <option value="Субота">Субота</option>
                        </select>
                        {/* <div className="mb-3">
                            <label for="timeToGo">timeToGo</label>
                            <input name="timeToGo" id="timeToGo" type="date" value={this.state.timeToGo} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div>
                        <div className="mb-3">
                            <label for="timeToCome">timeToCome</label>
                            <input name="timeToCome" id="timeToCome" type="date" value={this.state.timeToCome} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div> */}
                        <div className="mb-3">
                            <label for="trainType">trainType</label>
                            <input name="trainType" id="trainType" type="number" value={this.state.trainType} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div>
                        {/* <div className="mb-3">
                            <label for="description">description</label>
                            <textarea name="description" id="description" value={this.state.description} onChange={this.inputHandler.bind(this)} className="form-control" ></textarea>
                        </div> */}
                        {/* {this.renderValidationErrors()} */}
                        <Button bsStyle="info" onClick={this.submitHandler.bind(this)}>Update train</Button>
                        <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
                    </Col>
                </Row>
            </Grid>
        )
    }

}

export default AdminUpdateTrain;