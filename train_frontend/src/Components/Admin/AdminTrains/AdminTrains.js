import React, { Component } from 'react';
// import Aux from '../../../hoc/Aux/Aux';
import TrainCard from '../../TrainCard/TrainCard';
import axios from '../../../utils/axios';

class AdminTrains extends Component {
    state = {
        trains: []
    }

    componentDidMount() {
        axios.get('trainfantastic/trains')
            .then(response => {
                console.log(response);
                this.setState({
                    trains: response.data
                });
            })
            .catch(err => {
                console.log(err);
            })
    }

    renderTrains() {
        const { trains } = this.state;
        if(trains.length > 0) {
            return trains.map(train => {
                return <TrainCard train={train} />
            });
        }
    }

    render() {
        console.log(this.state.trains);
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-9">
                        <div className="row">
                           {this.renderTrains()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AdminTrains;