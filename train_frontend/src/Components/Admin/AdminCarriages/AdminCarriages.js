import React , { Component } from 'react';
import axios from '../../../utils/axios';
import Sidebar from '../../Layout/Sidebar/Sidebar';
import { NavLink } from 'react-router-dom';

class AdminCarriages extends Component {
    state = {
        carriages: []
    }

    componentDidMount() {
        const { trainId } = this.props.match.params;
        axios.get(`/trainfantastic/train/${trainId}/carriages`)
            .then(response => {
                console.log(response);
                this.setState({
                    carriages: response.data
                });
            })
            .catch(err => {
                console.log(err);
            })
    }

    renderTable() {
        return (
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">image</th>
                        <th scope="col">price</th>
                        <th scope="col">sits count</th>
                        <th scope="col"> --- </th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRaws()}
                </tbody>
            </table>
        );
    }

    renderRaws() {
        const { carriages } = this.state;
        if(carriages.length > 0) {
           return carriages.map((carriage,key) => {
               return (
                <tr key={key}>
                    <th scope="col">{carriage.carriageNo}</th>
                    <th scope="col">station start</th>
                    <th scope="col">{carriage.price}</th>
                    <th scope="col">{carriage.sitsCount}</th>
                    <th scope="col">
                        <NavLink className="btn btn-outline-primary" to={`carriage/${carriage.carriageId}`}>
                            carriage detail
                        </NavLink>
                    </th>
                </tr>
               )
           })
        }
    }

    render() {
        return (
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12">
                                {this.renderTable()}
                            </div>
                        </div>
                    </div>
                    <Sidebar />
                </div>
            </div>
        )
    }
}

export default AdminCarriages;