import React , { Component } from 'react';
import axios from '../../../utils/axios';
import Aux from '../../../hoc/Wrapper/Wrapper';
import {
    Row,
    Col,
    Grid
} from 'react-bootstrap'

class AdminCarriageAdd extends Component {
    state = {
        carriageNo: '',
        trainNo: '',
        carriageType: '',
    }

    inputHandler(event) {
        const { target } = event;
        this.setState({
            [target.name]: target.value
        });
    }

    submitHandler(event) {
        event.preventDefault();
        console.log(this.state);
        alert(localStorage.getItem('credentials'));
        axios.post('trainfantastic/manager/create/carriage', this.state, {
            headers: {
                'Authorization': `Basic ${localStorage.getItem('credentials')}`,
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                console.log(response)
                alert('successfully added train')
            })
            .catch(err => {
                console.log(err);
            })
    }

    componentWillMount() {
        console.log(this.props)
        this.setState({
            trainNo: this.props.match.params.trainId
        })
    }


    render() {
        return (
            <Grid>
            <Row>
                <Col md={6} className="offset-md-3">
                    <img class="mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
                    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
                    <div className="mb-3">
                        <label for="carriageNo">carriage Number</label>
                        <input name="carriageNo" id="carriageNo" type="number" value={this.state.carriageNo} onChange={this.inputHandler.bind(this)} className="form-control" />
                    </div>
                    <div className="mb-3">
                        <label for="trainNo">train number</label>
                        <input name="trainNo" id="trainNo" type="number" value={this.state.trainNo} onChange={this.inputHandler.bind(this)} className="form-control" />
                    </div>
                    <div className="mb-3">
                        <p>{this.state.trainNo}</p>
                    </div>

                    <div className="mb-3">
                        <label for="carriageType">carriageType</label>
                        <input name="carriageType" id="carriageType" type="number" value={this.state.carriageType} onChange={this.inputHandler.bind(this)} className="form-control" />
                    </div>
                    {/* {this.renderValidationErrors()} */}
                    <button class="btn btn-lg btn-outline-success btn-block" onClick={this.submitHandler.bind(this)} >Save carriage</button>
                    <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
                </Col>
            </Row>
        </Grid>
        )
    }
}

export default AdminCarriageAdd;