import React , {Component } from 'react';
import axios from '../../../utils/axios';
import {
    Row,
    Col,
    Grid,
    Button
} from 'react-bootstrap';

class CreateManager extends Component {
    state = {
        firstname: '',
        lastname: '',
        secondname: '',
        email: '',
        password: '',
        birthday: '',
        role: ''
    }

    inputHandler(event) {
        const { target } = event;
        this.setState({
            [target.name]: target.value
        });
    }

    submitHandler(event) {
        event.preventDefault();
        console.log(this.state);
        alert(123);
        axios.post('trainfantastic/admin/create/user', this.state, {
            headers: {
                'Authorization': `Basic ${localStorage.getItem('credentials')}`
            }
        })
            .then(response => {
                console.log('response', response)
                this.props.history.push('/admin/');
            })
            .catch(err => {
                console.log('err', err);
            })
    }

    render() {
        console.log(this.state);
        return (
            <Grid>
                <Row>
                    <Col md={6} className="offset-md-3">
                        <div className="mb-3">
                            <label htmlFor="firstname">first name</label>
                            <input name="firstname" id="firstname" type="text" value={this.state.firstname} onChange={this.inputHandler.bind(this)} className="htmlForm-control" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="lastname">last name</label>
                            <input name="lastname" id="lastname" type="text" value={this.state.lastname} onChange={this.inputHandler.bind(this)} className="htmlForm-control" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="secondname">second name</label>
                            <input name="secondname" id="secondname" type="text" value={this.state.secondname} onChange={this.inputHandler.bind(this)} className="htmlForm-control" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="email">email</label>
                            <input name="email" id="email" type="email" value={this.state.email} onChange={this.inputHandler.bind(this)} className="htmlForm-control" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="password">password</label>
                            <input name="password" id="password" type="password" value={this.state.password} onChange={this.inputHandler.bind(this)} className="htmlForm-control" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="role">role</label>
                            <input name="role" id="role" type="role" value={this.state.role} onChange={this.inputHandler.bind(this)} className="htmlForm-control" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="birthday">birthday</label>
                            <input name="birthday" id="birthday" type="date" value={this.state.birthday} onChange={this.inputHandler.bind(this)} className="htmlForm-control" />
                        </div>
                        {/* {this.renderValidationErrors()} */}
                        <button onClick={this.submitHandler.bind(this)}>Create user</button>
                        <p className="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
                    </Col>
                </Row>
            </Grid>
        )
    }
}

export default CreateManager;