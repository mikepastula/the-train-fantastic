import React , { Component } from 'react';
import axios from '../../../utils/axios';
import Aux from '../../../hoc/Wrapper/Wrapper';
import {
    Row,
    Col,
    Grid
} from 'react-bootstrap';

class AdminTrainAdd extends Component {
    state = {
        trainNo: null,
        stationFrom: null,
        stationTo: null,
        trainType: null,
        days: []
    }

    inputHandler(event) {
        const { target } = event;
        this.setState({
            [target.name]: target.value
        });
    }

    submitHandler(event) {
        event.preventDefault();
        console.log(this.state);
        axios.post('trainfantastic/manager/create/train', this.state, {
            headers: {
                'Authorization': `Basic ${localStorage.getItem('credentials')}`,
                'Access-Control-Allow-Headers': 'Authorization, Content-Type'
            }
        })
            .then(response => {
                console.log(response)
                alert('successfully added train');
                this.props.history.push('/');
            })
            .catch(err => {
                console.log(err);
            })
    }

    render() {
        return (
            <Grid>
            <Row>
                <Col md={6} className="offset-md-3">
                    <img class="mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
                    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
                    <div className="mb-3">
                        <label for="trainNo">train Number</label>
                        <input name="trainNo" id="trainNo" type="number" value={this.state.trainNo} onChange={this.inputHandler.bind(this)} className="form-control" />
                    </div>
                    <div className="mb-3">
                        <label for="stationFrom">stationFrom</label>
                        <input name="stationFrom" id="stationFrom" type="text" value={this.state.stationFrom} onChange={this.inputHandler.bind(this)} className="form-control" />
                    </div>
                    <div className="mb-3">
                        <label for="stationTo">stationTo</label>
                        <input name="stationTo" id="stationTo" type="text" value={this.state.stationTo} onChange={this.inputHandler.bind(this)} className="form-control" />
                    </div>
                    <select onChange={this.inputHandler.bind(this)} className="form-control" name="days" multiple>
                            <option value="Понеділок">Понеділок</option>
                            <option value="Середа">Сеереда</option>
                            <option value="Пятниця">пятниця</option>
                            <option value="Неділя">Неділя</option>
                        </select>
                    <select onChange={this.inputHandler.bind(this)} className="form-control" name="days" multiple>
                        <option value="Вівторок">Вівторок</option>
                        <option value="Четврег">Четврег</option>
                        <option value="Субота">Субота</option>
                    </select>
                    <div className="mb-3">
                        <label for="trainType">trainType</label>
                        <input name="trainType" id="trainType" type="number" value={this.state.trainType} onChange={this.inputHandler.bind(this)} className="form-control" />
                    </div>
                    {/* {this.renderValidationErrors()} */}
                    <button class="btn btn-lg btn-outline-success btn-block" onClick={this.submitHandler.bind(this)} >Create train</button>
                    <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
                </Col>
            </Row>
        </Grid>
        )
    }
}

export default AdminTrainAdd;