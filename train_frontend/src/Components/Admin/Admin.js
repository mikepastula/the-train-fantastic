import React , { Component } from 'react';
import Aux from '../../hoc/Wrapper/Wrapper';
import axios from '../../utils/axios';
import AdminTrainCard from '../Admin/AdminTrainCard/AdminTrainCard';
import { NavLink } from 'react-router-dom';

class Admin extends Component {
    state = {
        trains: [],
        role: '',
        isLoggedIn: false
    }

    componentDidMount() {
        const credentials = localStorage.getItem('credentials');
        if(!credentials) {
            alert('Unauthorized user !');
            this.props.history.push('/');
        }
        axios.get('trainfantastic/login', {
            headers: {
                'Authorization' : `Basic ${credentials}`
            }
        })
            .then(response => {
                console.log(response);
                let { role } = response.data;
                if(role.toLowerCase() !== 'manager' && role.toLowerCase() !== 'admin') {
                    return this.props.history.push('/');
                }
                this.setState({
                    role: role.toLowerCase(),
                    isLoggedIn: true
                })

            })
            .catch(err => {
                console.log(err);
                alert(JSON.stringify(err));
                this.props.history.push('/');
            })

            axios.get('trainfantastic/trains')
            .then(response => {
                console.log(response);
                this.setState({
                    trains: response.data
                });
            })
            .catch(err => {
                console.log(err);
        })
    }

    renderTrains() {
        const { trains } = this.state;
        if(trains.length > 0) {
            return trains.map(train => {
                return <AdminTrainCard train={train} />
            });
        }
    }

    render() {
        const { role } = this.state;
        return (
            <Aux>

                <h1>Hello admin</h1>
                <div className="form-group">
                    <NavLink className="btn btn-outline-success" to='/admin/train/add'>Add Train</NavLink>
                    <NavLink className="btn btn-outline-success" to='/admin/trains'>Manage Trains</NavLink>
                    {role === 'admin' ? <div>
                        <NavLink className="btn btn-outline-success" to='/admin/manager/create'>Create manager</NavLink>
                        <NavLink className="btn btn-outline-success" to='/admin/managers'>List of managers</NavLink>
                    </div> : null}
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-9">
                            <div className="row">
                            {this.renderTrains()}
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
        )
    }
}

export default Admin;