import React , { Component } from 'react';
import axios from '../../utils/axios';
import Aux from '../../hoc/Wrapper/Wrapper';

class EnsureLoggedIn extends React.Component {
    state = {
        isLoggedIn: false
    }

    componentDidMount() {
        const credentials = localStorage.getItem('credentials');
        if(!credentials) {
            alert('Unauthorized user !');
            this.props.history.push('/');
        }
        axios.get('trainfantastic/login', {
            headers: {
                'Authorization' : `Basic ${credentials}`
            }
        })
            .then(response => {
                console.log(response);
                let { role } = response.data;
                if(role.toLowerCase() !== 'manager' && role.toLowerCase() !== 'admin') {
                    return this.props.history.push('/');
                }
                this.setState({
                    isLoggedIn: true
                })

            })
            .catch(err => {
                console.log(err);
                alert(JSON.stringify(err));
                this.props.history.push('/');
            })
    }

    render() {
        const { isLoggedIn } = this.state;
        if(isLoggedIn) {
            return this.props.children;
        } else {
            return null
        }

    }
  }

  export default EnsureLoggedIn;
