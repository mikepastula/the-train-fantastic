import React from 'react';
import {
    Row,
    Col,
    Grid
} from 'react-bootstrap';
import stationImg from './station.jpg';

export default (props) => {
    return (
        <Grid>
        <Row>
            <Col md={8}>
                <img src={stationImg} alt="Chernivstsi station" />
                <br />
                <br />
                <h3 className="alert alert-info">За будь-якими питаннями звертайтеьс на головнум 246а, 11 поверх 11 кабінет.</h3>
            </Col>
        </Row>
    </Grid>
    )
}