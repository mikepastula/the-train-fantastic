import React, { Component } from 'react';
import axios from '../../utils/axios';
import {
    Row,
    Col,
    Grid
} from 'react-bootstrap';


class Account extends Component {
    state = {
        email: '',
        firstname: '',
        lastname: '',
        secondname: '',
        tickets: []
    }

    isAuthorized() {
        return axios.get('trainfantastic/login', {
            headers: {
                'Authorization': `Basic ${localStorage.getItem('credentials')}`
            }
        })
    }

    componentDidMount() {
        this.isAuthorized()
            .then(response => {
               const {firstname, lastname, secondname } = response.data;
                axios.get('trainfantastic/my/tickets', {
                    headers: {
                        'Authorization': `Basic ${localStorage.getItem('credentials')}`
                    }
                })
                    .then(response => {
                        this.setState({
                            firstname,
                            lastname,
                            secondname,
                            tickets: response.data
                        });
                    })
                    .catch(err => {
                        console.log('err', err);
                    })
            })
            .catch(err => {
                alert('Please login !!!');
                this.props.history.push('/');
            })
    }

    removeTicket(event) {
        const { id } = event.target.dataset;
        axios.delete(`trainfantastic/my/tickets/${id}`, {
            headers: {
                'Authorization': `Basic ${localStorage.getItem('credentials')}`
            }

        })
            .then(response => {
                alert('successfully remove ticket');
            })
            .catch(err => {
                console.log(err);
            })
    }

    renderRaws() {
        const { tickets } = this.state;
        if(tickets.length > 0) {
           return tickets.map((ticket, key) => {
               return (
                <tr key={key}>
                    <th scope="col">{ticket.sitNo}</th>
                    <th scope="col">{ticket.trainNo} </th>
                    <th scope="col">{ticket.carriageNo}</th>
                    <th scope="col">{ticket.dayGo}</th>
                    <th scope="col">{ticket.dayCome}</th>
                    <th scope="col">{ticket.where}</th>
                    <th scope="col">{ticket.from}</th>
                    <th scope="col">{ticket.price}</th>
                    <th scope="col"><button onClick={this.removeTicket.bind(this)} data-id={ticket.ticketId} className="btn btn-danger">remove</button></th>
                </tr>
               )
           })
        }
    }

    renderComponents() {
        return (
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Station number</th>
                        <th scope="col">Train number</th>
                        <th scope="col">Carriage number</th>
                        <th scope="col">Time to go</th>
                        <th scope="col">Time to come</th>
                        <th scope="col">From</th>
                        <th scope="col">To</th>
                        <th scope="col">price</th>
                        <th scope="col" >delete</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRaws()}
                </tbody>
            </table>
        );
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col md={8}>
                        {this.renderComponents()}
                    </Col>
                </Row>
            </Grid>
        )
    }
}

export default Account;