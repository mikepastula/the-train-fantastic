import React from 'react';
import Carousel from 'nuka-carousel';
import train from './img/train.jpg';
import train1 from './img/train1.png';
import train2 from './img/train2.jpeg';
import train3 from './img/train3.jpg';

export default (props) => {
    return (
        <Carousel>
            <img src={train} />
            <img src={train1} />
            <img src={train2} />
            <img src={train3} />
      </Carousel>
    )
}