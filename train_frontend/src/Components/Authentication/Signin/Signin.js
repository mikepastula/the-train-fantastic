import React, { Component} from 'react';
import axios from '../../../utils/axios';
import Aux from '../../../hoc/Wrapper/Wrapper';

class Signin extends Component {
    state = {
        email: '',
        password: ''
    }

    inputHandler(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    submitHandler(event) {
        event.preventDefault();
        const {password, email} = this.state;
        const encodedCredentials = btoa(email + ':'+ password);
        axios.get('trainfantastic/login', {
            headers: {
                'Authorization': `Basic ${encodedCredentials}`
            }
        })
            .then(response => {
                console.log(response);
                alert(`Successfully logged in as ${response.data.role}`)
                localStorage.setItem('credentials', encodedCredentials);
            })
            .catch(err => {
                console.log(err);
            });

    }

    render() {
        return (
            <Aux>
                <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                <div className="mb-3">
                    <label for="email">Email</label>
                    <input name="email" id="email" type="email" value={this.state.email} onChange={this.inputHandler.bind(this)} className="form-control" />
                </div>
                <div className="mb-3">
                    <label for="password">password</label>
                    <input name="password" id="password" type="password" value={this.state.password} onChange={this.inputHandler.bind(this)} className="form-control" />
                </div>
                <button className="btn btn-outline-primary btn-block" onClick={this.submitHandler.bind(this)}>Sign in</button>
            </Aux>
        )
    }
}

export default Signin;