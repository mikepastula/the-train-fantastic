import React , { Component } from 'react';
import axios from '../../../utils/axios';
import {
    Row,
    Col,
    Grid
} from 'react-bootstrap'

class Signup extends Component {
    state = {
        firstname: '',
        lastname: '',
        secondname: '',
        birthdat: '',
        email: '',
        validationErrors: []
    }

    inputHandler(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    submitHanlder(event) {
        event.preventDefault();
        const { email, firstname, lastname, secondname } = this.state;
        let errors = []
        console.log(this.state);

        if(!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email)) {
            errors.push('Invalid email');
        }

        if(firstname.length < 2) {
            errors.push('First name should be grater than 2 characters');
        }

        if(lastname.length < 2) {
            errors.push('Last name should be grater than 2 characters');
        }

        if(secondname.length < 2) {
            errors.push('Second name should be grater than 2 characters');
        }

        if(errors.length === 0) {
        axios.post('signup  ', this.state)
            .then(response => {
                alert('successfully sign up, check email for password');
                localStorage.setItem('credentials', btoa(email))
                this.props.history.push('/');
            })
            .catch(err => {
                console.log(err);
            })
        }
        return this.setState({
            validationErrors: errors
        })



    }

    renderValidationErrors() {
        const { validationErrors } = this.state;
        if(validationErrors.length > 0) {
            return validationErrors.map((error,key) => {
                return (
                    <p className="alert alert-danger" key={key}>{error}</p>
                )
            })
        }
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col md={6} className="offset-md-3">
                        <img class="mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
                        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
                        <div className="mb-3">
                            <label for="firstname">First Name</label>
                            <input name="firstname" id="firstname" type="text" value={this.state.firstname} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div>
                        <div className="mb-3">
                            <label for="lastname">Last Name</label>
                            <input name="lastname" id="lastname" type="text" value={this.state.lastname} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div>
                        <div className="mb-3">
                            <label for="secondname">Second Name</label>
                            <input name="secondname" id="secondName" type="text" value={this.state.secondname} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div>
                        <div className="mb-3">
                            <label for="email">email</label>
                            <input name="email" id="name" type="email" value={this.state.email} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div>
                        <div className="mb-3">
                            <label for="birthday">Day of birth</label>
                            <input name="birthday" id="birthday" type="date" value={this.state.birthday} onChange={this.inputHandler.bind(this)} className="form-control" />
                        </div>
                        {this.renderValidationErrors()}
                        <button class="btn btn-lg btn-outline-success btn-block" onClick={this.submitHanlder.bind(this)} >Sign up</button>
                        <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
                    </Col>
                </Row>
            </Grid>

        )
    }
}

export default Signup;