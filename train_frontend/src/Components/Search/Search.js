import React , { Component } from 'react';
import axios from '../../utils/axios';
import Aux from '../../hoc/Wrapper/Wrapper';
import { NavLink } from 'react-router-dom';

class Search extends Component {
    state = {
        stationFrom: '',
        stationTo: '',
        date: '',
        stations: [],
        sits: []
    }

    componentDidMount() {
        axios.get('trainfantastic/stations')
            .then(response => {
                const { data } = response;
                const stations = data.map(station => {
                    return station.stationName;
                });
                let from, to
                if(stations.length > 0) {
                    from = stations[0];
                    to = stations[0]
                }
                this.setState({
                    stations: stations,
                    stationFrom: from,
                    stationTo: to,
                })
            })
            .catch(err => {
                console.log(err);
            });
    }

    handleChange(event) {
        console.log(event.target.value);
        console.log(event.target.name);
        const { target } = event;
        this.setState({
            [target.name]: target.value
        });
    }

    handleSubmit() {
        console.log(this.state);
        axios.post('trainfantastic/getAvailableTickets',this.state)
            .then(response => {
                this.setState({
                    sits: response.data
                });
            })
            .catch(err => {
                console.log(err);
            })
    }

    handleClick(event) {
        const { target } = event;
        this.setState({
            [target.name]: target.value
        });
    }

    checkUser() {
        return axios.get('trainfantastic/login', {
            headers: {
                'Authorization': `Basic ${localStorage.getItem('credentials')}`
            }
        })
    }


    buyTicket(event) {
        console.log(event.target.dataset);
        const { sitno } = event.target.dataset;
        const { sits } = this.state;
        alert(sitno);
        this.checkUser()
            .then(response => {
                const sitObj = sits.find(function(sit) {
                    return sit.sitNo == sitno;
                });
                const { stationFrom , stationTo, date} = this.state;
                sitObj.dayGo = date;
                delete sitObj.timeCome;
                delete sitObj.timeGo;
                delete sitObj.price;
                let finnalData = {
                    ...sitObj,
                    stationFrom,
                    stationTo,
                }
                console.log(finnalData);
                if(finnalData) {
                    console.log(`Basic ${localStorage.getItem('credentials')}`);
                    axios.put('trainfantastic/my/addTicket',finnalData, {
                        headers: {
                            'Authorization': `Basic ${localStorage.getItem('credentials')}`,
                            'Access-Control-Allow-Headers': 'Authorization, Content-Type'
                        }
                    })
                        .then(response => {
                            console.log(response);
                        })
                        .catch(err => {
                            console.log(err);
                        })
                }
            })
            .catch(err => {
                alert('Not authorized user can\'t but a ticket');
                console.log(err);
            });
    }

    renderRaws() {
        const { sits } = this.state;
        if(sits.length > 0) {
           return sits.map((sit,key) => {
               return (
                <tr key={key}>
                    <th scope="col">{sit.sitNo}</th>
                    <th scope="col">{sit.trainNo} </th>
                    <th scope="col">{sit.carriageNo}</th>
                    <th scope="col">{sit.timeGo}</th>
                    <th scope="col">{sit.timeCome}</th>
                    <th scope="col">{sit.price}</th>
                    <th scope="col"><button onClick={this.buyTicket.bind(this)} data-sitNo={sit.sitNo} className="btn btn-success">buy</button></th>
                </tr>
               )
           })
        }
    }

    renderComponents() {
        return (
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Station number</th>
                        <th scope="col">Train number</th>
                        <th scope="col">Carriage number</th>
                        <th scope="col">Time to go</th>
                        <th scope="col">Time to come</th>
                        <th scope="col">price</th>
                        <th scope="col" >buy</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRaws()}
                </tbody>
            </table>
        );
    }

    render() {
        console.log(this.state);
        const { stations }= this.state;
        return (
            <Aux>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="#">Select options</a>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                        <li class="nav-item active">
                        <select onChange={this.handleChange.bind(this)} name="stationFrom">
                            {stations.length  > 0 ? stations.map(station => {
                                return (
                                    <option value={station}>{station}</option>
                                )
                            }) : null}
                        </select>
                        </li>
                        <li class="nav-item">
                        <select onChange={this.handleChange.bind(this)} name="stationTo">
                            {stations.length  > 0 ? stations.map(station => {
                                return (
                                    <option value={station}>{station}</option>
                                )
                            }) : null}
                        </select>
                        </li>
                        <li class="nav-item">
                           <input onChange={this.handleChange.bind(this)} name="date" type="date"  className="form-control"/>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                        </li>
                        </ul>
                        <button onClick={this.handleSubmit.bind(this)} >filter</button>
                    </div>
                </nav>
                {this.renderComponents()}
            </Aux>
        )
    }
 }

 export default Search;