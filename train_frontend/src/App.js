import React, { Component } from 'react';
import logo from './logo.svg';
import Header from './Components/Layout/Header/Header';
import Main from './Components/Main/Main';
import Admin from './Components/Admin/Admin';
import AdminTrains from './Components/Admin/AdminTrains/AdminTrains';
import Footer from './Components/Layout/Footer/Footer';
import Aux from './hoc/Wrapper/Wrapper';
import Carriages from './Components/Carriages/Carriages';
import AdminTrainAdd from './Components/Admin/AdminTrainAdd/AdminTrainAdd';
import CreateManager from './Components/Admin/CreateManager/CreateManager';
import AdminCarriageAdd from './Components/Admin/AminCarriageAdd/AdminCarriageAdd';
import AdminCarriages from './Components/Admin/AdminCarriages/AdminCarriages';
import Account from './Components/Account/Account';
import FeedBack from './Components/FeedBack/FeedBack';
import EnsureLoggedIn from './Components/EnsureLoggedIn/EnsureLoggedIn';
import Search from './Components/Search/Search';
import AdminUpdateTrain from './Components/Admin/AdminTrainUpdate/AdminTrainUpdate';
import Signup from './Components/Authentication/Signup/Signup';
// import '../public/css/bootstrap.min.css';
import { Switch, Route } from 'react-router-dom';
import './App.css';

class App extends Component {
  render() {
    return (
      <Aux>
        <Header/>
        <Switch>
          <Route exact path='/' component={Main} />
          <Route exact path='/admin' component={Admin}/>
          <Route path='/search' component={Search}/>
          <Route path='/account' component={Account}/>
          <Route path='/feedback' component={FeedBack}/>
          <Route path='/admin/train/add' component={AdminTrainAdd}/>
          <Route path='/admin/manager/create' component={CreateManager}/>
          <Route path='/admin/train/update/:trainId' component={AdminUpdateTrain}/>
          <Route path='/admin/carriage/add/:trainId' component={AdminCarriageAdd}/>
          <Route path='/admin/train/:trainId' component={AdminCarriages}/>
          {/* <Route path='/admin/train/:trainId' component={AdminTrainCreate}/> */}
          <Route exact path='/signup' component={Signup} />
          <Route path='/train/:id' component={Carriages} />
        </Switch>
        <Footer/>
      </Aux>
    );
  }
}

export default App;
