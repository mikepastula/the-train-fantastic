import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://fdff1e6f.ngrok.io',
    headers: {
        'Content-Type': 'application/json'
    }
});

export default instance;